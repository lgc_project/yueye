package com.mjgy.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mjgy.entity.ContactWayWhiteList;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月7日
* @version V1.0
*/
public interface ContactWayWhiteListRepository extends JpaRepository<ContactWayWhiteList, Long>, JpaSpecificationExecutor<ContactWayWhiteList> {
	
	void deleteByUserIdOrCheckerId(Long userId, Long checkerId);
	
	@Modifying
	@Query(value = "DELETE FROM contact_way_white_list WHERE TIME_TO_SEC(TIMEDIFF(NOW(), create_time))/86400 >= 7", nativeQuery = true)
	@Transactional
	void deleteOverdueChecker();
	
	@Transactional
	void deleteByType(Integer type);
	
	Integer countByUserIdAndCheckerId(Long userId, Long checkerId);
	
	Integer countByUserIdAndCheckerIdAndType(Long userId, Long checkerId, Integer type);
	
	Integer countByCheckerIdAndType(Long checkerId, Integer type);

}
