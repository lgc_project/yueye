package com.mjgy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.UserPhotoEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月28日
* @version V1.0
*/
public interface UserPhotoRepository extends JpaRepository<UserPhotoEntity, Long> {

	List<UserPhotoEntity> findByUser(MUserEntity user);
	
	List<UserPhotoEntity> findByUserAndLevel(MUserEntity user, int level);
}
