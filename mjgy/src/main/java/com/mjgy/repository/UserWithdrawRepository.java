package com.mjgy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mjgy.entity.UserWithdrawEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
public interface UserWithdrawRepository extends JpaRepository<UserWithdrawEntity, Long> {

	@Query(value = "SELECT COUNT(1) FROM e_user_withdraw uw LEFT JOIN e_user usr ON uw.user_id = usr.id " +
			"WHERE ((uw.status = :status AND :status = 0) OR (uw.status != 0 AND :status != 0))" +
			"AND (usr.id = :id OR :id IS null OR :id = '')" +
			"AND (LEFT(uw.apply_time, 10) >= :startTime OR :startTime IS null OR :startTime = '')" +
			"AND (LEFT(uw.apply_time, 10) <= :endTime OR :endTime IS null OR :endTime = '')" +
			"AND usr.city LIKE CONCAT('%', CONCAT(:city, '%')) " +
			"AND usr.nickname LIKE CONCAT('%', CONCAT(:nickname, '%'))", nativeQuery = true)
	Long countByStatusAndCityAndNickname(@Param("status") Integer status, @Param("id") Long id, @Param("city") String city, 
			@Param("nickname") String nickname, @Param("startTime") String startTime, @Param("endTime") String endTime);
	
	List<UserWithdrawEntity> findByUserId(Long userId);
}
