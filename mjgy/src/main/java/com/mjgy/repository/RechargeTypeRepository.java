package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mjgy.entity.RechargeTypeEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月13日
* @version V1.0
*/
public interface RechargeTypeRepository extends JpaRepository<RechargeTypeEntity, Long> {
	
}
