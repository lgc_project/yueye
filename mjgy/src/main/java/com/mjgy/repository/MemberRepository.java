package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mjgy.entity.MemberEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月11日
* @version V1.0
*/
public interface MemberRepository extends JpaRepository<MemberEntity, Long> {

	MemberEntity findByUserId(Long userId);
}
