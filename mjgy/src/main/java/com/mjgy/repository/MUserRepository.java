package com.mjgy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mjgy.entity.MUserEntity;

/**
* @Description: 用户持久类
* @author LGC
* @date 2018年7月11日
* @version V1.0
*/
public interface MUserRepository extends JpaRepository<MUserEntity, Long>, JpaSpecificationExecutor<MUserEntity> {

	MUserEntity findOneByUsername(String username);
	
	List<MUserEntity> findByProvinceAndCity(String province, String city);
	
	@Query(value = "select * from e_user where id != :userId and province = :province and city = :city and sex = :sex order by register_time desc", nativeQuery = true)
	List<MUserEntity> getNewUserInfo(@Param("userId") Long userId, @Param("province") String province, @Param("city") String city, @Param("sex") int sex);
	
	MUserEntity findOneByIdAndProvinceAndCityAndSex(Long userId, String province, String city, int sex);
	
	List<MUserEntity> findByProvinceAndCityAndSexAndIdentity(String province, String city, int sex, int identity);
	
	MUserEntity findTopById(Long userId);
}
