package com.mjgy.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mjgy.entity.CheckWhiteList;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月6日
* @version V1.0
*/
public interface CheckWhiteListRepository extends JpaRepository<CheckWhiteList, Long>, JpaSpecificationExecutor<CheckWhiteList> {

	void deleteByUserIdOrCheckerId(Long userId, Long checkerId);
	
	@Modifying
	@Query(value = "DELETE FROM check_white_list WHERE TIME_TO_SEC(TIMEDIFF(NOW(), create_time))/86400 >= 15", nativeQuery = true)
	@Transactional
	void deleteOverdueChecker();
	
	Integer countByUserIdAndCheckerId(Long userId, Long checkerId);
}
