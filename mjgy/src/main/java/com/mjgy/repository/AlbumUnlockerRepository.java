package com.mjgy.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mjgy.entity.AlbumUnlocker;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月7日
* @version V1.0
*/
public interface AlbumUnlockerRepository extends JpaRepository<AlbumUnlocker, Long>, JpaSpecificationExecutor<AlbumUnlocker> {

	@Modifying
	@Query(value = "DELETE FROM album_unlocker WHERE TIME_TO_SEC(TIMEDIFF(NOW(), create_time))/86400 >= 7", nativeQuery = true)
	@Transactional
	void deleteOverdueUnlocker();
	
	@Transactional
	void deleteByType(Integer type);
	
	Integer countByUserIdAndUnlockerId(Long userId, Long unlockerId);
	
	Integer countByUnlockerIdAndType(Long unlockerId, Integer type);
	
	void deleteByUserIdOrUnlockerId(Long userId, Long unlockerId);
	
}
