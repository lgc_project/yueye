package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mjgy.entity.UserReportEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月28日
* @version V1.0
*/
public interface UserReportRepository extends JpaRepository<UserReportEntity, Long> {
	
	void deleteByActiveIdOrPassiveId(Long activeId, Long passiveId);

	@Query(value = "SELECT COUNT(1) FROM e_user_report rpt JOIN e_user usr ON rpt.passive_id = usr.id" +
			" WHERE usr.sex = :sex" +
			" AND usr.city LIKE CONCAT('%', CONCAT(:city, '%'))" +
			" AND (usr.identity = :identity OR :identity IS NULL OR :identity = '')" +
			" AND usr.nickname LIKE CONCAT('%', CONCAT(:nickname, '%'))", nativeQuery = true)
	Long countBySex(@Param("sex") Integer sex, @Param("city") String city, @Param("identity") Integer identity, 
			@Param("nickname") String nickname);
	
}
