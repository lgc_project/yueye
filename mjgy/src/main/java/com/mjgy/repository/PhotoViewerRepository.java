package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mjgy.entity.PhotoViewerEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
public interface PhotoViewerRepository extends JpaRepository<PhotoViewerEntity, Long> {
	
	void deleteByUserIdOrViewerId(Long userId, Long viewerId);

	public void deleteByUserIdAndType(Long userId, Integer type);
	
	@Query(value = "SELECT COUNT(DISTINCT(viewer_id)) FROM photo_viewer WHERE user_id = :userId", nativeQuery = true)
	public Integer countViewerByUserId(@Param("userId") Long userId);
	
	public PhotoViewerEntity findTopByUserIdAndPhotoIdAndViewerId(Long userId, Long photoId, Long viewerId);

}
