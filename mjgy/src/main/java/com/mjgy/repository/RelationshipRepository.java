package com.mjgy.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mjgy.entity.UserAccessEntity;
import com.mjgy.entity.UserDefriendEntity;
import com.mjgy.entity.UserEvaluateEntity;
import com.mjgy.entity.UserFollowEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月14日
* @version V1.0
*/
public interface RelationshipRepository extends JpaRepository<UserFollowEntity, Long>, JpaSpecificationExecutor<UserFollowEntity> {

	/**
	 * 获取关注实体类
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	UserFollowEntity findOneByActiveIdAndPassiveId(Long activeId, Long passiveId);
	
	/**
	 * 获取拉黑实体类
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	@Query(value = "select new UserDefriendEntity(id, activeId, passiveId, defriend) from UserDefriendEntity where active_id = :activeId and passive_id = :passiveId")
	UserDefriendEntity findDefriendByActiveIdAndPassiveId(@Param("activeId") Long activeId, @Param("passiveId") Long passiveId);
	
	/**
	 * 获取关注数从高到低的用户
	 * 
	 * @return
	 */
	@Query(value = "select t.passive_id from (select passive_id, count(passive_id) as c from e_user_follow group by passive_id) t where t.passive_id is not null order by t.c desc", nativeQuery = true)
	List<BigInteger> followSum();
	
	@Query(value = "select t.passive_id from (select passive_id, count(passive_id) as c from e_user_follow group by passive_id) t where t.passive_id is not null order by t.c desc limit :page, :limit", nativeQuery = true)
	List<BigInteger> followSum(@Param("page") int page, @Param("limit") int limit);
	
	/**
	 * 关注列表
	 * 
	 * @param userId
	 * @return
	 */
	@Query(value = "select passive_id from e_user_follow where active_id = :userId and follow = 1", nativeQuery = true)
	List<BigInteger> getFollowList(@Param("userId") Long userId);
	
	/**
	 * 拉黑列表
	 * 
	 * @param userId
	 * @return
	 */
	@Query(value = "select passive_id from e_user_defriend where active_id = :userId and defriend = 1", nativeQuery = true)
	List<BigInteger> getDefriendList(@Param("userId") Long userId);
	
	/**
	 * 获取主动拉黑的列表
	 * 
	 * @param userId
	 * @return
	 */
	@Query("select ud.passiveId from UserDefriendEntity ud where ud.activeId = :userId and ud.defriend = 1")
	List<Long> getAccessDefriendList(@Param("userId") Long userId);
	
	@Query("select ud.passiveId from UserDefriendEntity ud where ud.activeId = :userId and ud.defriend = 1")
	Page<Long> getAccessDefriendList(@Param("userId") Long userId, Pageable pageable);
	
	/**
	 * 获取被拉黑人的列表
	 * 
	 * @param userId
	 * @return
	 */
	@Query("select ud.activeId from UserDefriendEntity ud where ud.passiveId = :userId and ud.defriend = 1")
	List<Long> getPassiveDefriendList(@Param("userId") Long userId);
	
	@Query("select ud.activeId from UserDefriendEntity ud where ud.passiveId = :userId and ud.defriend = 1")
	Page<Long> getPassiveDefriendList(@Param("userId") Long userId, Pageable pageable);
	
	/**
	 * 获取访问实体类
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	@Query(value = "select new UserAccessEntity(id, activeId, passiveId) from UserAccessEntity where active_id = :activeId and passive_id = :passiveId")
	UserAccessEntity findAccessByActiveIdAndPassiveId(@Param("activeId") Long activeId, @Param("passiveId") Long passiveId);
	
	/**
	 * 访客列表
	 * 
	 * @param userId
	 * @return
	 */
	@Query(value = "select active_id from e_user_access where passive_id = :userId order by update_time desc", nativeQuery = true)
	List<BigInteger> getAccessList(@Param("userId") Long userId);
	
	/**
	 * 获取评价
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	@Query(value = "from UserEvaluateEntity where activeId = :activeId and passiveId = :passiveId")
	UserEvaluateEntity findEvaluateByActiveIdAndPassiveId(@Param("activeId") Long activeId, @Param("passiveId") Long passiveId);
	
	/**
	 * 查看自己的评价
	 * 
	 * @param passiveId
	 * @return
	 */
	@Query(value = "from UserEvaluateEntity where passiveId = :passiveId")
	List<UserEvaluateEntity> findEvaluateByPassivaId(@Param("passiveId") Long passiveId);
}
