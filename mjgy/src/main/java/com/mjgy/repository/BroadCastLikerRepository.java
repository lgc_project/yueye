package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mjgy.entity.BroadCastLikerEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年9月15日
* @version V1.0
*/
public interface BroadCastLikerRepository extends JpaRepository<BroadCastLikerEntity, Long>, JpaSpecificationExecutor<BroadCastLikerEntity> {

	void deleteByLikerId(Long likerId);
	
	void deleteByBroadCastId(Long broadCastId);
	
	Integer countByBroadCastIdAndLikerId(Long broadCastId, Long likerId);
}
