package com.mjgy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mjgy.entity.DeviceCodeEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月11日
* @version V1.0
*/
public interface DeviceCodeRepository extends JpaRepository<DeviceCodeEntity, Long>, JpaSpecificationExecutor<DeviceCodeEntity> {

	List<DeviceCodeEntity> findByUserId(Long userId);
	
	List<DeviceCodeEntity> findByDeviceCode(String deviceCode);
	
	DeviceCodeEntity findOneByUserIdAndDeviceCode(Long userId, String deviceCode);
	
}
