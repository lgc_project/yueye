package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mjgy.entity.InviteCodeEntity;
import com.mjgy.entity.MUserEntity;

/**
* @Description: TODO
* @author admin
* @date 2018年7月12日
* @version V1.0
*/
public interface InviteCodeRepository extends JpaRepository<InviteCodeEntity, Long>, JpaSpecificationExecutor<InviteCodeEntity> {

	InviteCodeEntity findOneByUser(MUserEntity user);
	
	InviteCodeEntity findOneByInviteCode(String inviteCode);
}
