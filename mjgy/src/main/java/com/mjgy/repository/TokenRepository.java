package com.mjgy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mjgy.entity.TokenEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月19日
* @version V1.0
*/
public interface TokenRepository extends JpaRepository<TokenEntity, Long> {

	TokenEntity findByUserId(Long userId);
	
	TokenEntity findByToken(String token);
}
