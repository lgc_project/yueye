package com.mjgy.service;

import com.common.utils.R;

/**
* @Description: LGC
* @author LGC
* @date 2018年12月13日
* @version V1.0
*/
public interface ZodiacService {

	/**
	 * 评价
	 * 
	 * @param zodiacId
	 * @param userId
	 * @param content
	 * @return
	 */
	R evaluate(Long zodiacId, Long userId, String content);
	
	/**
	 * 评价列表
	 * 
	 * @param zodiacId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	R evaluateList(Long zodiacId, Integer page, Integer pageSize);
	
	/**
	 * 星座运势
	 * 
	 * @param name
	 * @param type
	 * @return
	 */
	R getZodiacFortune(String name, String type);
	
	/**
	 * 笑话大全
	 * 
	 * @param sort
	 * @param page
	 * @param pageSize
	 * @param time
	 * @return
	 */
	R getJuhe(Integer page, Integer pageSize, String time);
}
