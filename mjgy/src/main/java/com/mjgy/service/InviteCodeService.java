package com.mjgy.service;

import com.common.utils.PageUtils;
import com.common.utils.Query;
import com.common.utils.R;

/**
* @Description: TODO
* @author admin
* @date 2018年7月12日
* @version V1.0
*/
public interface InviteCodeService {

	PageUtils queryPage(Query query);
	
	/**
	 * 生成邀请码
	 * 
	 * @param id
	 * @return
	 */
	R buildCode(Long id);
	
	/**
	 * 申请邀请码
	 * 
	 * @param userId
	 * @param type
	 * @return
	 */
	R applyCode(Long userId, int type);
	
	/**
	 * 激活二维码
	 * 
	 * @param userId
	 * @return
	 */
	R activateCode(Long userId, String inviteCode);
}
