package com.mjgy.service;

import java.util.List;
import java.util.Map;

/**
* @Description: TODO
* @author quanlq
* @date 2018年11月6日
* @version V1.0
*/
public interface UserReportService {

	/**
	 * 根据性别查找用户举报列表
	 * @param sex	性别
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	List<Map<String, Object>> findListBySex(Integer sex, String city, Integer identity, String nickname, Integer page, Integer size);
	
	/**
	 * 统计举报列表记录数
	 * @param sex	性别
	 * @param city	城市
	 * @param identity	身份
	 * @param nickname	昵称
	 * @return
	 */
	Long countBySex(Integer sex, String city, Integer identity, String nickname);
	
}
