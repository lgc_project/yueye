package com.mjgy.service;

import com.common.utils.R;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月2日
* @version V1.0
*/
public interface NoticeService {

	/**
	 * 保存客服信息
	 * 
	 * @param title
	 * @param content
	 * @return
	 */
	R saveNotice(String title, String content);
	
	/**
	 * 更新客服信息
	 * 
	 * @param title
	 * @param content
	 * @return
	 */
	R updateNotice(Long noticeId, String title, String content);
	
	/**
	 * 获取客服信息
	 * 
	 * @return
	 */
	R getNotice();
}
