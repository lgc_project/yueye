package com.mjgy.service;

import com.common.utils.PageUtils;
import com.common.utils.Query;
import com.common.utils.R;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月14日
* @version V1.0
*/
public interface RelationshipService {

	/**
	 * 关注
	 * 
	 * @param activeId 关注者
	 * @param passiveId 被关注者
	 * @param follow 是否关注
	 * @return
	 */
	R follow(Long activeId, Long passiveId, boolean follow);
	
	/**
	 * 关注列表
	 * 
	 * @param userId
	 * @return
	 */
	R followList(Long userId);
	
	/**
	 * 拉黑
	 * 
	 * @param activeId 拉黑人
	 * @param passiveId 被拉黑人
	 * @param defriend 是否拉黑
	 * @return
	 */
	R defriend(Long activeId, Long passiveId, boolean defriend);
	
	/**
	 * 拉黑列表
	 * 
	 * @param userId
	 * @return
	 */
	R defriendList(Long userId);
	
	/**
	 * 访问
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	R access(Long activeId, Long passiveId);
	
	/**
	 * 历史访客
	 * 
	 * @param userId
	 * @return
	 */
	R accessList(Long userId);
	
	/**
	 * 评价
	 * 
	 * @param activeId
	 * @param passiveId
	 * @param content
	 * @return
	 */
	R evaluate(Long activeId, Long passiveId, String content);
	
	/**
	 * 获取评价内容
	 * 
	 * @param userId
	 * @return
	 */
	R evaluateList(Long userId);
	
	/**
	 * 获取评价别人的内容
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	R evaluateContent(Long activeId, Long passiveId);
	
	/**
	 * 评价列表
	 * 
	 * @param query
	 * @return
	 */
	PageUtils queryEvaluatePage(Query query);
}
