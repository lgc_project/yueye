package com.mjgy.service;
/**
* @Description: 定时任务
* @author LGC
* @date 2018年9月30日
* @version V1.0
*/
public interface JobService {

	/**
	 * 会员天数减一任务
	 * 凌晨12：00执行
	 */
	public void memberRunDay();
	
}
