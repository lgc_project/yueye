package com.mjgy.service;

import java.util.List;
import java.util.Map;

import com.mjgy.entity.PayLog;

/**
* @Description: 支付记录
* @author quanlq
* @date 2018年10月9日
* @version V1.0
*/
public interface PayLogService {

	/**
	 * 保存支付记录
	 * @param payLog 支付记录实体
	 */
	void save(PayLog payLog);
	
	/**
	 * 收入记录统计数据
	 * @param startDate	开始日期
	 * @param endDate	结束日期
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	List<Map<String, Object>> statistic(String startDate, String endDate, Integer page, Integer size);
	
	/**
	 * 根据城市统计收入记录
	 * @param date	日期
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	List<Map<String, Object>> statisticByCity(String date, Integer page, Integer size);
	
}
