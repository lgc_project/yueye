package com.mjgy.service;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.common.utils.R;
import com.mjgy.entity.ContactWayWhiteList;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月12日
* @version V1.0
*/
/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public interface PayService {
	
	R getWXPayOrder(HttpServletRequest request, Long userId, Long collecterId, double payMoney, int type, Long rechargeId, Long serviceId);
	
	String wxPayNotify(HttpServletRequest request, HttpServletResponse response);
	
	R signContactWay(Long checkerId, Long userId, BigDecimal payMoney);
	
	R signPayPhoto(Long userId, Long viewerId, Long photoId, BigDecimal payMoney);
	
	R signAlbum(Long userId, Long unlockerId);
	
	/**
	 * 历史消费
	 * 
	 * @param params
	 * @return
	 */
	R payLogInfo(Map<String, Object> params);
	
	/**
	 * 收入记录
	 * 
	 * @param params
	 * @return
	 */
	R incomeInfo(Map<String, Object> params); 
}
