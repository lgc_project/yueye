package com.mjgy.service;

import com.mjgy.entity.PhotoViewerEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
public interface PhotoViewerService {
	
	public void save(PhotoViewerEntity photoViewerEntity);

	/**
	 * 一键恢复阅后即焚
	 * 
	 * @param userId	用户ID
	 */
	public void deleteByUserIdAndType(Long userId, Integer type);
	
	/**
	 * 统计有多少人焚毁了用户的照片
	 * 
	 * @param userId
	 * @return
	 */
	public Integer countViewerByUserId(Long userId);
	
}
