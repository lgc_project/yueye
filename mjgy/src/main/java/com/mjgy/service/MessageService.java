package com.mjgy.service;

import java.util.List;
import java.util.Map;

import com.mjgy.entity.MessageEntity;

/**
* @Description: 消息中心Service
* @author quanlq
* @date 2018年9月1日
* @version V1.0
*/
public interface MessageService {
	
	/**
	 * 获取我的未读消息总数
	 * 
	 * @param userId 用户ID
	 * @param isChecked 是否已查看：0=否，1=是
	 */
	Integer countByUserIdAndIsChecked(Long userId, int isChecked);

	/**
	 * 获取最新消息及对应类型的未读消息数
	 * 
	 * @return
	 */
	Map<String, Object> findNewestMessages(Long userId);
	
	/**
	 * 根据消息类型获取消息列表
	 * 
	 * @param userId 用户ID
	 * @param type 消息类型：0=查看申请，1=系统消息，2=收益提醒，3=评价通知
	 * @param page 页码
	 * @param size 每页条数
	 * @return
	 */
	List<Map<String, Object>> findByUserIdAndType(Long userId, String type, Integer page, Integer size);
	
	/**
	 * 发送一条消息
	 * @param messageEntity
	 */
	void save(MessageEntity messageEntity);
	
	/**
	 * 根据消息ID更新是否允许查看字段
	 * @param isAllowed	是否已通过查看验证：0=已拒绝，1=已通过，2=未处理
	 * @param id	消息ID
	 */
	void updateIsAllowedById(Integer isAllowed, Long id);
	
	/**
	 * 根据消息ID更新发送照片是否焚毁字段
	 * @param id	消息ID
	 */
	void updateIsBurnedById(Long id);
	
}
