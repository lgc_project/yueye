package com.mjgy.service;

import java.util.List;

import com.mjgy.entity.DeviceCodeEntity;
import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.TokenEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月19日
* @version V1.0
*/
public interface TokenService {

	/**
	 * 保存或更新token值
	 * 
	 * @param userId
	 */
	String saveOrUpdateToken(Long userId);
	
	/**
	 * 根据用户ID获取token实体
	 * 
	 * @param userId
	 * @return
	 */
	TokenEntity getTokenByUserId(Long userId);
	
	/**
	 * 根据token值获取token实体
	 * 
	 * @param token
	 * @return
	 */
	TokenEntity getToken(String token);
	
	MUserEntity getUserById(Long id);
	
	List<DeviceCodeEntity> findByUserId(Long id);

}
