package com.mjgy.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.common.utils.R;
import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.PhotoViewerEntity;
import com.mjgy.entity.UserPhotoEntity;
import com.mjgy.repository.PhotoViewerRepository;
import com.mjgy.repository.UserPhotoRepository;
import com.mjgy.service.UserPhotoService;

/**
* @Description: TODO
* @author LGC
* @date 2018年8月1日
* @version V1.0
*/
@Service("userPhotoService")
@Transactional
public class UserPhotoServiceImpl implements UserPhotoService {

	@Autowired
	private EntityManager em;
	@Autowired
	private UserPhotoRepository upr;
	@Autowired
	private PhotoViewerRepository pvr;
	
	@Override
	public R saveUserPhoto(Long userId, int level, BigDecimal checkPay, String imagePath) {
		MUserEntity user = em.find(MUserEntity.class, userId);
		if(user == null) {
			return R.error(202, "用户不存在");
		}
		
		List<UserPhotoEntity> userPhotos = upr.findByUserAndLevel(user, level);
		for(UserPhotoEntity photo : userPhotos) {
			em.remove(photo);
		}
		String[] imagePaths = imagePath.split(",");
		for(String img : imagePaths) {
			UserPhotoEntity userPhoto = new UserPhotoEntity();
			userPhoto.setImagePath(img);
			userPhoto.setLevel(level);
			userPhoto.setCheckPay(checkPay);
			userPhoto.setCheckStatus(true);
			userPhoto.setCreateTime(new Date());
			userPhoto.setUser(user);
			userPhoto.setStatus(false);
			em.persist(userPhoto);
		}

		return R.success();
	}

	@Override
	public R updateUserPhoto(Long photoId) {
		Date curDate = new Date();
		UserPhotoEntity userPhoto = em.find(UserPhotoEntity.class, photoId);
		if(userPhoto == null) {
			return R.error(202, "图片不存在");
		}
		userPhoto.setLevel(1);
		userPhoto.setUpdateTime(curDate);
		
		return R.success();
	}

	@Override
	public R getUserPhoto(Long userId, Long viewerId) {
		MUserEntity user = em.find(MUserEntity.class, userId);
		if(user == null) {
			return R.error(202, "用户不存在");
		}
		
		List<UserPhotoEntity> list = upr.findByUser(user);
		
		for(UserPhotoEntity photo : list) {
			if(userId.longValue() == viewerId.longValue()) {
				continue;
			}
			Long photoId = photo.getId();
			if(photo.getLevel() == 1){	//阅后即焚图片状态默认为true可看
				photo.setCheckStatus(true);
			}else if(photo.getLevel() == 2){	//红包图片状态默认为false不可看
				photo.setCheckStatus(false);
			}
			PhotoViewerEntity photoViewer = pvr.findTopByUserIdAndPhotoIdAndViewerId(userId, photoId, viewerId);
			if(photoViewer != null) {
				photo.setCheckStatus(!photo.getCheckStatus());
			}
		}
		
		return R.success().put("data", list);
	}

	@Override
	public R deleteUserPhoto(Long photoId) {
		UserPhotoEntity photo = em.find(UserPhotoEntity.class, photoId);
		if(photo != null) {
			em.remove(photo);
		}
		return R.success();
	}

	@Override
	public R editPhotoStatus(Long photoId, boolean status) {
		UserPhotoEntity photo = em.find(UserPhotoEntity.class, photoId);
		if(photo != null) {
			photo.setStatus(status);
			em.merge(photo);
		}
		return R.success();
	}

}
