package com.mjgy.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mjgy.entity.PhotoViewerEntity;
import com.mjgy.repository.PhotoViewerRepository;
import com.mjgy.service.PhotoViewerService;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
@Service
public class PhotoViewerServiceImpl implements PhotoViewerService{
	
	@Autowired
	private PhotoViewerRepository photoViewerRepository;
	
	@Override
	public void save(PhotoViewerEntity photoViewerEntity) {
		photoViewerRepository.save(photoViewerEntity);
	}

	@Override
	@Transactional
	public void deleteByUserIdAndType(Long userId, Integer type) {
		photoViewerRepository.deleteByUserIdAndType(userId, type);
	}

	@Override
	public Integer countViewerByUserId(Long userId) {
		return photoViewerRepository.countViewerByUserId(userId);
	}

}
