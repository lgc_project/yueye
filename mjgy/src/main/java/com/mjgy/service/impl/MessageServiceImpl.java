package com.mjgy.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.MessageEntity;
import com.mjgy.repository.MUserRepository;
import com.mjgy.repository.MessageRepository;
import com.mjgy.service.MessageService;

/**
* @Description: 消息中心Service实现
* @author quanlq
* @date 2018年9月1日
* @version V1.0
*/
@Service("messageService")
public class MessageServiceImpl implements MessageService{
	
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private MUserRepository mUserRepository;

	@Override
	public Integer countByUserIdAndIsChecked(Long userId, int isChecked) {
		return messageRepository.countByUserIdAndIsChecked(userId, isChecked);
	}

	@SuppressWarnings("rawtypes")
	@Override
	@Transactional
	public List<Map<String, Object>> findByUserIdAndType(Long userId, String type, Integer page, Integer size) {
		messageRepository.updateIsCheckedByUserIdAndType(userId, type);
		List rows = messageRepository.findByUserIdAndType(userId, type, (page-1)*size, size);
		List<Map<String, Object>> list = new ArrayList<>();
		for(Object row : rows){
			Object[] cells = (Object[]) row;
			Map<String, Object> map = new HashMap<>();
			map.put("id", cells[0]);
			map.put("userId", cells[1]);
			map.put("type", cells[2]);
			map.put("senderId", cells[3]);
			map.put("content", cells[4]);
			map.put("sendTime", cells[5]);
			map.put("isChecked", cells[6]);
			map.put("isAllowed", cells[7]);
			map.put("sendImage", cells[8]);
			map.put("isBurned", cells[9]);
			map.put("senderName", cells[10]);
			map.put("avatar", cells[11]);
			list.add(map);
		}
		return list;
	}
	
	@Override
	public Map<String, Object> findNewestMessages(Long userId) {
		Map<String, Object> resultMap = new HashMap<>();
		//获取4种消息类型的数据
		for(int i=0; i<4; i++){
			Map<String, Object> temp = new HashMap<>();
			temp.put("unCheckedNum", messageRepository.countByUserIdAndTypeAndIsChecked(userId, i+"", 0));
			
			MessageEntity messageEntity = messageRepository.findTopByUserIdAndTypeOrderBySendTimeDesc(userId, i+"");
			if(messageEntity != null && messageEntity.getSenderId() != null){
				MUserEntity mUserEntity = mUserRepository.findOne(messageEntity.getSenderId());
				if(mUserEntity != null){					
					messageEntity.setSenderName(mUserEntity.getNickname());
				}
			}
			temp.put("newestMessage", messageEntity);
			resultMap.put(""+i, temp);
		}
		
		return resultMap;
	}

	@Override
	public void save(MessageEntity messageEntity) {
		messageRepository.save(messageEntity);
	}

	@Override
	@Transactional
	public void updateIsAllowedById(Integer isAllowed, Long id) {
		messageRepository.updateIsAllowedById(isAllowed, id);
	}

	@Override
	@Transactional
	public void updateIsBurnedById(Long id) {
		messageRepository.updateIsBurnedById(id);
	}
	
	
}
