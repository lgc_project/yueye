package com.mjgy.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.common.utils.R;
import com.mjgy.entity.RechargeTypeEntity;
import com.mjgy.repository.RechargeTypeRepository;
import com.mjgy.service.MemberService;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月13日
* @version V1.0
*/
@Service
@Transactional
public class MemberServiceImpl implements MemberService {

	@Autowired
	private RechargeTypeRepository rechargeTypeRepository;
	@Autowired
	private EntityManager em;
	@Override
	public R rechargeTypeList() {
		List<RechargeTypeEntity> list = rechargeTypeRepository.findAll();
		return R.success().put("data", list);
	}

	@Override
	public R editRechargeType(Map<String, Object> params) {
		if(params == null || params.isEmpty()) {
			return R.error("入参为空");
		}
		
//		Long id = (Long) params.get("id");
//		int rechargeDays = (int) params.get("rechargeDays");
//		BigDecimal rechargeAmount = (BigDecimal) params.get("rechargeAmount");
//		double dicount = (double) params.get("dicount");
		String id = (String) params.get("id");
		String rechargeDays = (String) params.get("rechargeDays");
		String rechargeAmount = (String) params.get("rechargeAmount");
		String dicount = (String) params.get("dicount");
		RechargeTypeEntity rt = em.find(RechargeTypeEntity.class, Long.valueOf(id));
		if(rt == null) {
			return R.error("没有找到套餐信息");
		}
		rt.setRechargeDays(Integer.valueOf(rechargeDays));
		rt.setRechargeAmount(new BigDecimal(rechargeAmount));
		rt.setDicount(Long.valueOf(dicount));
		rt.setUpdateTime(new Date());
		em.merge(rt);
		
		return R.success("修改成功");
	}

}
