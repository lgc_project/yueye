package com.mjgy.service.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.MemberEntity;
import com.mjgy.service.JobService;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月30日
* @version V1.0
*/
@Service
@Transactional
public class JobServiceImpl implements JobService {

	private Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void memberRunDay() {
		logger.error("-----------------MemberRunDay Method Start---------------------");
		StringBuilder sql = new StringBuilder();
		sql.append("from MemberEntity where surplusDays > 0");
		Query query = em.createQuery(sql.toString());
		List<MemberEntity> members = query.getResultList();
		for(MemberEntity member : members) {
			int surplusDays = member.getSurplusDays() - 1;
			// 如果剩余天数为0，则把用户设置成非会员状态。
			if(surplusDays == 0) {
				MUserEntity user = em.find(MUserEntity.class, member.getUserId());
				if(user != null) {
					user.setIdentity(3);
					em.merge(user);
				}
			}
			member.setSurplusDays(surplusDays);
			member.setUpdateTime(new Date());
			em.merge(member);
		}
		logger.error("-----------------MemberRunDay Method end-----------------------");
	}

}
