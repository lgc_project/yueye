package com.mjgy.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.common.utils.R;
import com.mjgy.config.SysConfig;
import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.ZodiacEvaluateEntity;
import com.mjgy.service.ZodiacService;
import com.mjgy.utils.HttpRequest;

import net.sf.json.JSONObject;

/**
* @Description: TODO
* @author LGC
* @date 2018年12月13日
* @version V1.0
*/
@Service("zodiacService")
@Transactional
public class ZodiacServiceImpl implements ZodiacService {

	@Autowired
	private EntityManager em;
	
	@Override
	public R evaluate(Long zodiacId, Long userId, String content) {
		ZodiacEvaluateEntity ze = new ZodiacEvaluateEntity();
		ze.setZodiacId(zodiacId);
		ze.setUserId(userId);
		ze.setContent(content);
		ze.setCreateTime(new Date());
		em.merge(ze);
		return R.success();
	}

	@Override
	public R evaluateList(Long zodiacId, Integer page, Integer pageSize) {
		if(page != null && pageSize != null) {
			page = (page - 1) * pageSize;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("from ZodiacEvaluateEntity where zodiacId = :zodiacId");
		Query query = em.createQuery(sql.toString());
		query.setParameter("zodiacId", zodiacId);
		if(page != null && pageSize != null) {
			query.setFirstResult(page);
			query.setMaxResults(pageSize);
		}
		List<ZodiacEvaluateEntity> zeList = query.getResultList();
		List<Map<String, Object>> result = new ArrayList<>();
		for(ZodiacEvaluateEntity ze : zeList) {
			Map<String, Object> map = new HashMap<>();
			Long userId = ze.getUserId();
			MUserEntity user = em.find(MUserEntity.class, userId);
			if(user != null) {
				map.put("nickname", user.getNickname());
				map.put("content", ze.getContent());
			}
			result.add(map);
;		}
		
		return R.success().put("data", result);
	}

	@Override
	public R getZodiacFortune(String name, String type) {
		String result = null;
		
		String url = "http://web.juhe.cn:8080/constellation/getAll";
		String key = SysConfig.zodiacKey;
		Map<String, Object> params = new HashMap<>();
		params.put("key", key);
		params.put("consName", name);
		params.put("type", type);
		String paramsStr = HttpRequest.urlencode(params);
		
		result = HttpRequest.sendGet(url, paramsStr);
		JSONObject obj = JSONObject.fromObject(result);
		if(obj.getInt("error_code") != 0) {
			return R.error().put("msg", obj.get("reason"));
		} else {
			return R.success().put("data", obj);
		}
	}

	@Override
	public R getJuhe(Integer page, Integer pageSize, String time) {
		String result = null;
		String url = "http://japi.juhe.cn/joke/content/list.from";
		Map<String, Object> params = new HashMap<>();
		params.put("sort", "desc");
		params.put("page", page);
		params.put("pageSize", pageSize);
		params.put("time", time);
		params.put("key", SysConfig.happyKey);
		String paramsStr = HttpRequest.urlencode(params);
		result = HttpRequest.sendGet(url, paramsStr);
		JSONObject obj = JSONObject.fromObject(result);
		if(obj.getInt("error_code") == 0) {
			return R.success().put("data", obj.get("result"));
		} else {
			return R.error().put(obj.get("error_code").toString(), obj.get("reason"));
		}
	}

}
