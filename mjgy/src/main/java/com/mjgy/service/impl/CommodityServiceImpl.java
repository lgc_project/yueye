package com.mjgy.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.common.utils.R;
import com.mjgy.entity.CommodityEntity;
import com.mjgy.service.CommodityService;

/**
* @Description: TODO
* @author lguochao
* @date 2019年1月9日
* @version V1.0
*/
@Service("commodityService")
@Transactional
public class CommodityServiceImpl implements CommodityService {

	@Autowired
	private EntityManager em;
	
	@Override
	public R getAllCommodity(Integer page, Integer pageSize) {
		if(page != null && pageSize != null) {
			page = (page - 1) * pageSize;
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append("from CommodityEntity");
		Query query = em.createQuery(sql.toString());
		if(page != null && pageSize != null) {
			query.setFirstResult(page);
			query.setMaxResults(pageSize);
		}
		List<CommodityEntity> result = query.getResultList();
		return R.success().put("data", result);
	}

}
