package com.mjgy.service;

import java.math.BigDecimal;

import com.common.utils.R;

/**
* @Description: TODO
* @author LGC
* @date 2018年8月1日
* @version V1.0
*/
public interface UserPhotoService {
	
	/**
	 * 保存上传的图片
	 * 
	 * @param userId
	 * @param level
	 * @param checkPay
	 * @param imagePath
	 * @return
	 */
	R saveUserPhoto(Long userId, int level, BigDecimal checkPay, String imagePath);
	
	/**
	 * 获取用户图库
	 * 
	 * @param userId
	 * @param viewerId
	 * @return
	 */
	R getUserPhoto(Long userId, Long viewerId);
	
	/**
	 * 设置图片为阅后即焚
	 * 
	 * @param photoId
	 * @return
	 */
	R updateUserPhoto(Long photoId);
	
	/**
	 * 删除用户图片
	 * 
	 * @param photoId
	 * @return
	 */
	R deleteUserPhoto(Long photoId);
	
	/**
	 * 编辑图片状态（封禁/解封）
	 * 
	 * @param photoId
	 * @param status
	 * @return
	 */
	R editPhotoStatus(Long photoId, boolean status);
}
