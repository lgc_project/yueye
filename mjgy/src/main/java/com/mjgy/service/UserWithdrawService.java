package com.mjgy.service;

import java.util.List;
import java.util.Map;

import com.common.utils.R;
import com.mjgy.entity.UserWithdrawEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
public interface UserWithdrawService {
	
	public void save(UserWithdrawEntity userWithdrawEntity);
	
	/**
	 * 根据状态获取提现列表
	 * 
	 * @param id	提现ID
	 * @param status	状态：0=待处理，1=已执行，2=已打回
	 * @param city	城市
	 * @param nickname	昵称
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	List<Map<String, Object>> findListByStatus(Integer status, Long id, String city, String nickname, 
			String startTime, String endTime, Integer page, Integer size);

	R dealUserWithdraw(Long id, Integer status);
	
}
