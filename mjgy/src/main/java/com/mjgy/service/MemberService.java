package com.mjgy.service;

import java.util.Map;

import com.common.utils.R;

/**
* @Description: TODO
* @author LGC
* @date 2018年10月13日
* @version V1.0
*/
public interface MemberService {
	
	/**
	 * 获取会员充值套餐
	 * 
	 * @return
	 */
	R rechargeTypeList();
	
	/**
	 * 更新会员套餐
	 * 
	 * @param params
	 * @return
	 */
	R editRechargeType(Map<String, Object> params);
}
