package com.mjgy.service;

import com.common.utils.R;

/*
 * @Description: 交易逻辑类
 * @Author: lguochao
 * @Date: 2019-01-13 00:13:08
 */
public interface DealService {
    
    /**
     * @description: 保存交易信息
     * @param {type} 
     * @return: 
     */
    R saveDeal(Long userId, Long commodityId, String receiver, String address, String phone);
    /**
     * @description: 获取用户订单列表
     * @param {type} 
     * @return: 
     */
    R getDealList(Long userId, Integer page, Integer pageSize);
}