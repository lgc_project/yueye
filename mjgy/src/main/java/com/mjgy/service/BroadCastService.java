package com.mjgy.service;

import java.util.List;
import java.util.Map;

import com.common.utils.R;
import com.mjgy.entity.BroadCastEntity;
import com.mjgy.entity.BroadCastLikerEntity;
import com.mjgy.entity.BroadCastSignerEntity;

/**
* @Description: TODO
* @author quanlq
* @date 2018年9月14日
* @version V1.0
*/
public interface BroadCastService {

	/**
	 * 保存约会广播
	 * 
	 * @param broadCastEntity 约会广播实体
	 */
	void saveBroadCast(BroadCastEntity broadCastEntity);
	
	/**
	 * 获取约会广播列表
	 * 
	 * @param sortKey 排序
	 * @param sex	性别：0=男，1=女
	 * @param city	城市
	 * @return
	 */
	List<Map<String, Object>> findList(String sortKey, Integer sex, String city, Long userId, Integer page, Integer size);
	
	/**
	 * 点赞约会广播
	 * 
	 * @param broadCastLikerEntity	点赞实体
	 * @return
	 */
	R like(BroadCastLikerEntity broadCastLikerEntity);
	
	/**
	 * 报名约会广播
	 * 
	 * @param broadCastSignerEntity	报名实体
	 * @return
	 */
	R signUp(BroadCastSignerEntity broadCastSignerEntity);
	
	/**
	 * 查看报名列表
	 * 
	 * @param broadCastId 约会广播ID
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	List<Map<String, Object>> findSignerList(Long broadCastId, Integer page, Integer size);
	
	/**
	 * 删除用户约会广播
	 * 
	 * @param userId 用户ID
	 */
	void updateStatus(Integer isDeleted, Long userId);
	
	/**
	 * 查看用户约会广播
	 * 
	 * @param userId 用户ID
	 * @return
	 */
	Map<String, Object> findByUserId(Long userId, Long viewerId);
	
	/**
	 * 查看约会广播详情
	 * 
	 * @param broadCastId 约会广播ID
	 * @return
	 */
	Map<String, Object> findById(Long broadCastId);
	
	/**
	 * 根据性别查看约会广播列表
	 * 
	 * @param sex	性别
	 * @param nickname	昵称
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	List<Map<String, Object>> findListBySex(Integer sex, String nickname, Integer page, Integer size);
	
	/**
	 * 根据性别统计数据总条数
	 * 
	 * @param sex 性别
	 * @return
	 */
	Long countBySex(Integer sex, String nickname);
	
}
