package com.mjgy.service;

import com.common.utils.R;

/**
* @Description: TODO
* @author lguochao
* @date 2019年1月9日
* @version V1.0
*/
public interface CommodityService {

	R getAllCommodity(Integer page, Integer pageSize);
}
