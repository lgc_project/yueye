package com.mjgy.service;

import java.math.BigDecimal;
import java.util.Map;

import com.common.utils.PageUtils;
import com.common.utils.Query;
import com.common.utils.R;
import com.mjgy.entity.MUserEntity;

/**
* @Description: TODO
* @author LGC
* @date 2018年7月11日
* @version V1.0
*/
public interface MUserService {

	/**
	 * 注册
	 * 
	 * @param phone
	 * @param password
	 * @return
	 */
	R register(String phone, String password, int sex, String applyCodeInfo);
	
	/**
	 * 更新用户的性别
	 * 
	 * @param userId
	 * @param sex
	 * @return
	 */
	R updateUserSex(Long userId, int sex);
	
	/**
	 * 上传头像
	 * 
	 * @param userId
	 * @param imagePath
	 * @return
	 */
	R uploadAvatar(Long userId, String imagePath);
	
	/**
	 * 忘记密码
	 * 
	 * @param phone
	 * @param newPassword
	 * @return
	 */
	R forgetPassword(String phone, String newPassword);
	
	/**
	 * 登录
	 * 
	 * @param username
	 * @param password
	 * @param deviceCode
	 * @return
	 */
	R login(String username, String password, String deviceCode);
	
	/**
	 * 完善信息
	 * 
	 * @param params
	 * @return
	 */
	R updateUserInfo(MUserEntity user);
	
	/**
	 * 绑定手机
	 * 
	 * @param userId
	 * @param phone
	 * @return
	 */
	R bindPhone(Long userId, String phone);
	
	/**
	 * 修改密码
	 * 
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	R updatePwd(Long userId, String oldPassword, String newPassword);
	
	/**
	 * 获取本省市内的用户列表
	 * 
	 * @param province
	 * @param city
	 * @param sex
	 * @param type
	 * @param page
	 * @param pageSize
	 * @return
	 */
	R getAllUserInfo(Long userId, String province, String city, int sex, int type, Integer page, Integer pageSize);
	
	/**
	 * 获取附近的人
	 * 
	 * @param userId
	 * @param lon 经度
	 * @param lat 纬度
	 * @return
	 */
	R getNearbyUserInfo(Long userId, BigDecimal lon, BigDecimal lat);
	
	/**
	 * 搜索
	 * 
	 * @param nickname 昵称（模糊查询）
	 * @param age 年龄（范围值）
	 * @param height 身高（范围值）
	 * @param weight 体重（范围值）
	 * @param cupSize 罩杯（固定值）
	 * @param occupation 职业（固定值）
	 * @param style 打扮风格（固定值）
	 * @param language 语言（固定值）
	 * @param relationship 情感（固定值）
	 * @param dationProgram 约会节目（多选）
	 * @param datingCodition 约会条件（多选）
	 * @return
	 */
	R searchUserInfo(String nickname, String age, String height, String weight, String cupSize, 
			String occupation, String style, String language, String relationship, String dationProgram, String datingCodition);
	
	/**
	 * 获取用户个人信息
	 * 
	 * @param userId
	 * @param lon 经度
	 * @param lat 纬度
	 * @return
	 */
	R getUserInfo(Long userId, Long viewerId);
	
	/**
	 * 更新用户的位置信息
	 *
	 * @param userId
	 * @param lon
	 * @param lat
	 * @return
	 */
	R updateUserLocation(Long userId, double lon, double lat);
	
	/**
	 * 更新用户的查看权限
	 * 
	 * @param userId	用户ID
	 * @param checkLimit	查看权限：0=公开，1=查看相册付费，2=查看前需要通过我的验证，3=隐身
	 * @return
	 */
	R updateCheckLimit(Long userId, Integer checkLimit);
	
	/**
	 * 设置付费相册金额
	 * 
	 * @param userId	用户ID
	 * @param albumAmount	付费相册设置金额
	 * @return
	 */
	R updateAlbumAmount(Long userId, BigDecimal albumAmount);
	
	/**
	 * 更新钱包金额
	 * 
	 * @param userId	用户ID
	 * @param amount	增加金额
	 * @return
	 */
	R updateWallet(Long userId, BigDecimal amount);
	
	/**
	 * 女性认证
	 * 
	 * @param userId
	 * @param identityPhoto
	 * @return
	 */
	R identity(Long userId, String identityPhoto);
	
	//----------------------------后台管理--------------------------------//
	/**
	 * 用户列表
	 * 
	 * @param query
	 * @return
	 */
	PageUtils queryPage(Query query);
	
	/**
	 * 设备码列表
	 * 
	 * @param query
	 * @return
	 */
	PageUtils queryDeviceCodePage(Query query);
	
	/**
	 * 封禁/解封 设备
	 * 
	 * @param id
	 * @param status
	 * @return
	 */
	R editDeviceCode(Long id, boolean status);
	
	/**
	 * 女性认证列表
	 * 
	 * @param query
	 * @return
	 */
	PageUtils queryIdentityPage(Query query);
	
	/**
	 * 是否通过认证
	 * 
	 * @param id
	 * @param identity
	 * @return
	 */
	R editIdentity(Long id, int identity);
	
	/**
	 * 用户详情
	 * 
	 * @param id
	 * @return
	 */
	R userDetail(Long id);
	
	/**
	 * 用户删除
	 * 
	 * @param id
	 * @return
	 */
	R userDel(Long id);
	
	/**
	 * 冻结/解冻
	 * 
	 * @param params
	 * @return
	 */
	R userFreezeOrThaw(Map<String, Object> params);
	
	R userAvatarFreeze(Long id);
}
