package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 允许某用户查看某用户个人信息（15天有效，查看权限为需要验证时启用）
* @author quanlq
* @date 2018年10月6日
* @version V1.0
*/
@Entity
@Table(name = "check_white_list")
public class CheckWhiteList implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户ID*/
	@Column(name = "user_id", nullable = true)
	private Long userId;
	
	/** 查看人ID*/
	@Column(name = "checker_id", nullable = true)
	private Long checkerId;
	
	/** 创建时间*/
	@Column(name = "create_time", nullable = true)
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(Long checkerId) {
		this.checkerId = checkerId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
