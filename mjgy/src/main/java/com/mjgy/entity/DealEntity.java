package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*
 * @Description: 交易类
 * @Author: lguochao
 * @Date: 2019-01-13 00:03:32
 */
@Entity
@Table(name = "e_deal")
public class DealEntity extends BaseEntity {
    private static final long serialVersionUID = -312772247308474086L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // 用户ID
    @Column
    private Long userId;
    // 商品ID
    @Column
    private Long commodityId;
    // 交易状态（1：交易进行中，2：交易已完成，3：交易取消）
    @Column
    private int state;
    // 收货人
    @Column
    private String receiver;
    // 地址
    @Column
    private String address;
    // 手机号
    @Column
    private String phone;
    @Transient
    private CommodityEntity commodity;
    /**
     * @return Long return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Long return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return int return the state
     */
    public int getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(int state) {
        this.state = state;
    }

    /**
     * @return String return the receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * @param receiver the receiver to set
     */
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    /**
     * @return String return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return String return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }


    /**
     * @return Long return the commodityId
     */
    public Long getCommodityId() {
        return commodityId;
    }

    /**
     * @param commodityId the commodityId to set
     */
    public void setCommodityId(Long commodityId) {
        this.commodityId = commodityId;
    }

	public CommodityEntity getCommodity() {
		return commodity;
	}

	public void setCommodity(CommodityEntity commodity) {
		this.commodity = commodity;
	}

}