package com.mjgy.entity;
/**
* @Description: TODO
* @author lguochao
* @date 2019年1月9日
* @version V1.0
*/

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "e_commodity")
public class CommodityEntity extends BaseEntity {

	private static final long serialVersionUID = 7054761389489775215L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	// 商品名称
	@Column
	private String name;
	// 商品图片
	@Column
	private String imgPath;
	// 价格
	@Column
	private double price;
	// 介绍
	@Column
	private String introduce;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	
	
}
