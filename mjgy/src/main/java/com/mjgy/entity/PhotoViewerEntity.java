package com.mjgy.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
@Entity
@Table(name = "photo_viewer")
public class PhotoViewerEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 被查看图片用户的ID*/
	@Column(name = "user_id")
	private Long userId;
	
	/** 查看图片用户的ID*/
	@Column(name = "viewer_id")
	private Long viewerId;
	
	/** 图片ID*/
	@Column(name = "photo_id")
	private Long photoId;
	
	/** 图片类型：1=阅后即焚图片，2=红包图片*/
	@Column(name = "type")
	private Integer type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getViewerId() {
		return viewerId;
	}

	public void setViewerId(Long viewerId) {
		this.viewerId = viewerId;
	}

	public Long getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
