package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 公告实体类
* @author LGC
* @date 2018年10月2日
* @version V1.0
*/
@Entity
@Table(name = "e_notice")
public class NoticeEntity extends BaseEntity {
	
	private static final long serialVersionUID = -1464625131027813590L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "title", length = 255, nullable = true)
	private String title;
	
	@Column(name = "content", length = 255, nullable = true)
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
