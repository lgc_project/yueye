package com.mjgy.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 约会广播和点赞用户的中间表
* @author quanlq
* @date 2018年9月14日
* @version V1.0
*/
@Entity
@Table(name = "broast_cast_liker")
public class BroadCastLikerEntity implements Serializable{

	private static final long serialVersionUID = -4029134794049862633L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 约会广播ID*/
	@Column(name = "broad_cast_id", nullable = false)
	private Long broadCastId;
	
	/** 点赞用户ID*/
	@Column(name = "liker_id", nullable = false)
	private Long likerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBroadCastId() {
		return broadCastId;
	}

	public void setBroadCastId(Long broadCastId) {
		this.broadCastId = broadCastId;
	}

	public Long getLikerId() {
		return likerId;
	}

	public void setLikerId(Long likerId) {
		this.likerId = likerId;
	}

	
}
