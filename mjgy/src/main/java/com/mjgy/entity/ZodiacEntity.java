package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 星座表
* @author LGC
* @date 2018年12月13日
* @version V1.0
*/
@Entity
@Table(name = "e_zodiac")
public class ZodiacEntity extends BaseEntity {

	private static final long serialVersionUID = -1109562194343140749L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", length = 10, nullable = true)
	private String name;
	
	@Column(name = "content", length = 255, nullable = true)
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
