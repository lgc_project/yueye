package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 用户举报
* @author quanlq
* @date 2018年10月28日
* @version V1.0
*/
@Entity
@Table(name = "e_user_report")
public class UserReportEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 举报人ID*/
	@Column(name = "active_id", length = 20, nullable = true)
	private Long activeId;
	
	/** 被举报人ID*/
	@Column(name = "passive_id", length = 20, nullable = true)
	private Long passiveId;
	
	/** 举报内容*/
	@Column(name = "content", nullable = true)
	private String content;
	
	/** 举报图片*/
	@Column(name = "images", nullable = true, columnDefinition = "text")
	private String images;
	
	/** 创建时间*/
	@Column(name = "create_time", nullable = true)
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getPassiveId() {
		return passiveId;
	}

	public void setPassiveId(Long passiveId) {
		this.passiveId = passiveId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
