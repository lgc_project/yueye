package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 评价实体类
* @author LGC
* @date 2018年10月8日
* @version V1.0
*/
@Entity
@Table(name = "e_user_evaluate")
public class UserEvaluateEntity extends BaseEntity {

	private static final long serialVersionUID = 2575752098678269636L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// 主动评价人
	@Column(name = "active_id", length = 20, nullable = true)
	private Long activeId;
	
	// 被动评价人
	@Column(name = "passive_id", length = 20, nullable = true)
	private Long passiveId;
	
	// 评价内容
	@Column(name = "content", length = 36, nullable = true)
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getPassiveId() {
		return passiveId;
	}

	public void setPassiveId(Long passiveId) {
		this.passiveId = passiveId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
