package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 用户关注表
* @author LGC
* @date 2018年9月14日
* @version V1.0
*/
@Entity
@Table(name = "e_user_follow")
public class UserFollowEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8273204395008332990L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// 主动人
	@Column(name = "active_id", length = 20, nullable = true)
	private Long activeId;
	
	// 被动人
	@Column(name = "passive_id", length = 20, nullable = true)
	private Long passiveId;
	
	// 关注
	@Column(name = "follow", length = 1, nullable = true)
	private boolean follow;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getPassiveId() {
		return passiveId;
	}

	public void setPassiveId(Long passiveId) {
		this.passiveId = passiveId;
	}

	public boolean isFollow() {
		return follow;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}
}
