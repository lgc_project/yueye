package com.mjgy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 支付记录表
* @author quanlq
* @date 2018年10月9日
* @version V1.0
*/
@Entity
@Table(name = "pay_log")
public class PayLog implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 支付人ID*/
	@Column(name = "payer_id")
	private Long payerId;
	
	/** 收款人ID*/
	@Column(name = "collecter_id")
	private Long collecterId;
	
	/** 购买的服务ID*/
	@Column(name = "service_id")
	private Long serviceId;
	
	/** 支付方式：0=微信，1=支付宝*/
	@Column(name = "pay_way")
	private Integer payWay;
	
	/** 支付金额*/
	@Column(name = "amount", scale = 2, precision = 10)
	private BigDecimal amount;
	
	/** 类型：0=查看联系方式，1=红包图片，2=付费相册，3=发广播，4=购买会员*/
	@Column(name = "type")
	private Integer type;
	
	/** 创建时间*/
	@Column(name = "create_time")
	private Date createTime;
	
	/** 备注*/
	@Column(name = "memo")
	private String memo;

	/** 状态：0=未支付，1=支付成功，2=支付失败*/
	@Column(name = "status")
	private Integer status;
	
	/**
	 * 订单号
	 */
	@Column(name = "out_trade_no")
	private String outTradeNo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPayerId() {
		return payerId;
	}

	public void setPayerId(Long payerId) {
		this.payerId = payerId;
	}

	public Long getCollecterId() {
		return collecterId;
	}

	public void setCollecterId(Long collecterId) {
		this.collecterId = collecterId;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getPayWay() {
		return payWay;
	}

	public void setPayWay(Integer payWay) {
		this.payWay = payWay;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}
	
	
}
