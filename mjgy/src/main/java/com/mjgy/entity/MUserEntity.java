package com.mjgy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* @Description: 用户实体类
* @author LGC
* @date 2018年7月10日
* @version V1.0
*/
@Entity
@Table(name = "e_user")
public class MUserEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// 用户父类ID
	@Column(name = "pid", length = 20, nullable = true)
	private Long pid;
	
	// 账号
	@Column(name = "username", length = 20, nullable = true)
	private String username;
	
	// 昵称
	@Column(name = "nickname", length = 20, nullable = true)
	private String nickname;
	
	// 省
	@Column(name = "province", length = 10, nullable = true)
	private String province;
	
	// 市
	@Column(name = "city", length = 10, nullable = true)
	private String city;
	
	// 身份（0：非会员 1：会员）
	@Column(name = "identity", length = 1, nullable = true)
	private int identity;
	
	// 性别（0：男 1：女）
	@Column(name = "sex", length = 1, nullable = true)
	private int sex = -1;
	
	// 历史消费
	@Column(name = "consume", scale = 2, precision = 10, nullable = true)
	private BigDecimal consume;
	
	// 电话
	@Column(name = "phone", length = 11, nullable = true)
	private String phone;
	
	// 密码
	@Column(name = "password", length = 100, nullable = true)
	private String password;
	
	// 密钥
	@Column(name = "salt", length = 100, nullable = true)
	private String salt;
	
	// 机器码
	@Column(name = "deviceCode", length = 36, nullable = true)
	private String deviceCode;
	
	// 注册时间
	@Column(name = "register_time", length = 10, nullable = true)
	private Date registerTime;
	
	// 最近登录时间
	@Column(name = "login_time", length = 10, nullable = true)
	private Date loginTime;
	
	// 新增时间
	@Column(name = "create_time", length = 10, nullable = true)
	private Date createTime;
	
	// 修改时间
	@Column(name = "update_time", length = 10, nullable = true)
	private Date updateTime;
	
	// 是否冻结
	@Column(name = "status", length = 1, nullable = true)
	private boolean status;
	
	// 头像
	@Column(name = "avatar", length = 100, nullable = true)
	private String avatar;
	
	// 头像是否封禁
	@Column(name = "avatar_status", length = 1, nullable = true)
	private boolean avatarStatus = true;
	
	// 年龄
	@Column(name = "age", length = 3, nullable = true)
	private int age;
	
	// 职业
	@Column(name = "occupation", length = 10, nullable = true)
	private String occupation;
	
	// 身高
	@Column(name = "height", length = 3, nullable = true)
	private int height;
	
	// 体重
	@Column(name = "weight", length = 3, nullable = true)
	private int weight;
	
	// 胸围
	@Column(name = "bust", length = 3, nullable = true)
	private int bust;
	
	// 罩杯
	@Column(name = "cup_size", length = 20, nullable = true)
	private String cupSize;
	
	// 打扮风格
	@Column(name = "style", length = 20, nullable = true)
	private String style;
	
	// 语言
	@Column(name = "language", length = 20, nullable = true)
	private String language;
	
	// 情感
	@Column(name = "relationship", length = 20, nullable = true)
	private String relationship;
	
	// 约会节目
	@Column(name = "dating_program", length = 20, nullable = true)
	private String datingProgram;
	
	// 约会条件
	@Column(name = "dating_codition", length = 20, nullable = true)
	private String datingCodition;
	
	//约会金额
	@Column(name = "dating_money")
	private BigDecimal datingMoney;

	// 微信
	@Column(name = "wechat", length = 20, nullable = true)
	private String wechat;
	
	// qq
	@Column(name = "qq", length = 20, nullable = true)
	private String qq;
	
	// 个人介绍
	@Column(name = "introduce", length = 20, nullable = true)
	private String introduce;
	
	// 申请邀请码信息，途径
	@Column(name = "apply_code_info", length = 255, nullable = true)
	private String applyCodeInfo;
	
	// 邀请码（一对多）
	// mappedBy：映射的字段
	// 用户---邀请码是双向一对一关系，解析JSON的时候，要加上JsonIgnore，不然会导致无限递归的问题
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	@JsonIgnore
	private List<InviteCodeEntity> inviteCodes;

	// 用户图库（一对多）
	// fetch：加载机制
	// 用户---图库是双向多对一，一对多关系，解析JSON的时候，要加上JsonIgnore，不然会导致无限递归的问题
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	@JsonIgnore
	private List<UserPhotoEntity> userPhotos;
	
	// 用户--位置
	@OneToOne(cascade = CascadeType.ALL, optional = true, mappedBy = "user")
	@JsonIgnore
	private UserLocationEntity location;
	
	/** 钱包余额*/
	@Column(name = "wallet", scale = 2, precision = 10, nullable = true, columnDefinition = "decimal(10, 2) default 0.00")
	private BigDecimal wallet; 
	
	/** 冻结的钱包余额*/
	@Column(name = "frozen_wallet", scale = 2, precision = 10, nullable = true)
	private BigDecimal frozenWallet;
	
	/** 查看权限：0=公开，1=查看相册付费，2=查看前需要通过我的验证，3=隐身*/
	@Column(name = "check_limit", nullable = true, columnDefinition = "int default 0")
	private Integer checkLimit;
	
	/** 付费相册设置金额*/
	@Column(name = "album_amount", scale = 2, precision = 10, nullable = true, columnDefinition = "decimal(10, 2) default 0.00")
	private BigDecimal albumAmount;
	
	// 女性认证图片
	@Column(name = "identity_photo")
	private String identityPhoto;
	
	// 女性认证码
	@Column(name = "identityCode")
	private String identityCode;
	
	//-------------------------非数据库字段----------------------------//
	@Transient
	private String memberSurplusDays;
	@Transient
	private double distance;
	@Transient
	private boolean isfollow;
	@Transient
	private boolean online;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getIdentity() {
		return identity;
	}

	public void setIdentity(int identity) {
		this.identity = identity;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public BigDecimal getConsume() {
		return consume;
	}

	public void setConsume(BigDecimal consume) {
		this.consume = consume;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getDeviceCode() {
		return deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public boolean isAvatarStatus() {
		return avatarStatus;
	}

	public void setAvatarStatus(boolean avatarStatus) {
		this.avatarStatus = avatarStatus;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getBust() {
		return bust;
	}

	public void setBust(int bust) {
		this.bust = bust;
	}

	public String getCupSize() {
		return cupSize;
	}

	public void setCupSize(String cupSize) {
		this.cupSize = cupSize;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getDatingProgram() {
		return datingProgram;
	}

	public void setDatingProgram(String datingProgram) {
		this.datingProgram = datingProgram;
	}

	public String getDatingCodition() {
		return datingCodition;
	}

	public void setDatingCodition(String datingCodition) {
		this.datingCodition = datingCodition;
	}

	public BigDecimal getDatingMoney() {
		return datingMoney;
	}

	public void setDatingMoney(BigDecimal datingMoney) {
		this.datingMoney = datingMoney;
	}

	public List<InviteCodeEntity> getInviteCodes() {
		return inviteCodes;
	}

	public void setInviteCodes(List<InviteCodeEntity> inviteCodes) {
		this.inviteCodes = inviteCodes;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getQq() {
		return qq;
	}
	
	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public List<UserPhotoEntity> getUserPhotos() {
		return userPhotos;
	}

	public void setUserPhotos(List<UserPhotoEntity> userPhotos) {
		this.userPhotos = userPhotos;
	}

	public UserLocationEntity getLocation() {
		return location;
	}

	public void setLocation(UserLocationEntity location) {
		this.location = location;
	}

	public BigDecimal getWallet() {
		return wallet;
	}

	public void setWallet(BigDecimal wallet) {
		this.wallet = wallet;
	}

	public BigDecimal getFrozenWallet() {
		return frozenWallet;
	}

	public void setFrozenWallet(BigDecimal frozenWallet) {
		this.frozenWallet = frozenWallet;
	}

	public Integer getCheckLimit() {
		return checkLimit;
	}

	public void setCheckLimit(Integer checkLimit) {
		this.checkLimit = checkLimit;
	}

	public BigDecimal getAlbumAmount() {
		return albumAmount;
	}

	public void setAlbumAmount(BigDecimal albumAmount) {
		this.albumAmount = albumAmount;
	}

	public String getApplyCodeInfo() {
		return applyCodeInfo;
	}

	public void setApplyCodeInfo(String applyCodeInfo) {
		this.applyCodeInfo = applyCodeInfo;
	}

	public String getIdentityPhoto() {
		return identityPhoto;
	}

	public void setIdentityPhoto(String identityPhoto) {
		this.identityPhoto = identityPhoto;
	}
	
	public String getIdentityCode() {
		return identityCode;
	}

	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}

	public String getMemberSurplusDays() {
		return memberSurplusDays;
	}

	public void setMemberSurplusDays(String memberSurplusDays) {
		this.memberSurplusDays = memberSurplusDays;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public boolean isIsfollow() {
		return isfollow;
	}

	public void setIsfollow(boolean isfollow) {
		this.isfollow = isfollow;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}
	
}
