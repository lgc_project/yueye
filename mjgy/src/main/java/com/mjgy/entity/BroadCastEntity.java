package com.mjgy.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
* @Description: 约会广播表
* @author quanlq
* @date 2018年9月13日
* @version V1.0
*/
@Entity
@Table(name = "broad_cast")
public class BroadCastEntity implements Serializable{

	private static final long serialVersionUID = -536947444814902030L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 广播类型：0=普通约会广播，1=报名约会广播*/
	@Column(name = "type", length = 10, nullable = true)
	private String type;
	
	/** 约会城市*/
	@Column(name = "city", length = 20, nullable = true)
	private String city;
	
	/** 约会日期*/
	@Column(name = "date", length = 10, nullable = true)
	private String date;
	
	/** 约会时间*/
	@Column(name = "time", length = 10, nullable = true)
	private String time;
	
	/** 约会要求*/
	@Column(name = "description", nullable = true)
	private String description;
	
	/** 广播图片*/
	@Column(name = "images", nullable = true, columnDefinition = "text")
	private String images;
	
	/** 封禁图片*/
	@Column(name = "forbid_images", nullable = true, columnDefinition = "text")
	private String forbidImages;
	
	/** 创建时间*/
	@Column(name = "create_time", length = 20, nullable = true)
	private String createTime;
	
	/** 点赞数*/
	@Column(name = "like_num", nullable = true, columnDefinition = "int default 0")
	private Integer likeNum;
	
	/** 报名数*/
	@Column(name = "sign_num", nullable = true, columnDefinition = "int default 0")
	private Integer signNum;
	
	/** 用户ID*/
	@Column(name = "user_id", nullable = false)
	private Long userId;
	
	/** 是否已删除：0=生效，1=用户已删除，2=运营已删除*/
	@Column(name = "is_deleted", nullable = false)
	private Integer isDeleted;
	
	/** 用户名*/
	@Transient
	private String nickname;
	
	/** 头像*/
	@Transient
	private String avatar;
	
	/** 性别：0=男，1=女*/
	@Transient
	private String sex;
	
	/** 身份：0=非会员 ，1=会员*/
	@Transient
	private String identity;
	
	/** 是否已点赞：0=否，1=是*/
	@Transient
	private String isLiker;
	
	/** 是否已报名：0=否，1=是*/
	@Transient
	private String isSigner;
	
	/** 是否约会中：0=否，1=是*/
	@Transient
	private Integer isDating;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getForbidImages() {
		return forbidImages;
	}

	public void setForbidImages(String forbidImages) {
		this.forbidImages = forbidImages;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(Integer likeNum) {
		this.likeNum = likeNum;
	}

	public Integer getSignNum() {
		return signNum;
	}

	public void setSignNum(Integer signNum) {
		this.signNum = signNum;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getIsLiker() {
		return isLiker;
	}

	public void setIsLiker(String isLiker) {
		this.isLiker = isLiker;
	}

	public String getIsSigner() {
		return isSigner;
	}

	public void setIsSigner(String isSigner) {
		this.isSigner = isSigner;
	}

	public Integer getIsDating() {
		return isDating;
	}

	public void setIsDating(Integer isDating) {
		this.isDating = isDating;
	}
	
}
