package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: TODO
* @author quanlq
* @date 2018年9月14日
* @version V1.0
*/
@Entity
@Table(name = "broast_cast_signer")
public class BroadCastSignerEntity implements Serializable{

	private static final long serialVersionUID = 2514404036361634293L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 约会广播ID*/
	@Column(name = "broad_cast_id", nullable = false)
	private Long broadCastId;
	
	/** 报名用户ID*/
	@Column(name = "signer_id", nullable = false)
	private Long signerId;
	
	/** 是否被约：0=否，1=是*/
	@Column(name = "is_dated", nullable = true, columnDefinition = "int default 0")
	private Integer isDated;
	
	/** 是否已阅：0=否，1=是*/
	@Column(name = "is_checked", nullable = true, columnDefinition = "int default 0")
	private Integer isChecked;
	
	/** 创建时间*/
	@Column(name = "create_time", nullable = true)
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBroadCastId() {
		return broadCastId;
	}

	public void setBroadCastId(Long broadCastId) {
		this.broadCastId = broadCastId;
	}

	public Long getSignerId() {
		return signerId;
	}

	public void setSignerId(Long signerId) {
		this.signerId = signerId;
	}

	public Integer getIsDated() {
		return isDated;
	}

	public void setIsDated(Integer isDated) {
		this.isDated = isDated;
	}

	public Integer getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Integer isChecked) {
		this.isChecked = isChecked;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
