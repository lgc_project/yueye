package com.mjgy.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
* @Description: 用户位置表（经纬度）
* @author LGC
* @date 2018年9月11日
* @version V1.0
*/
@Entity
@Table(name = "e_user_location")
public class UserLocationEntity extends BaseEntity {

	private static final long serialVersionUID = 6621490249768157378L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "user_id", referencedColumnName = "id", unique = true)
	private MUserEntity user;
	
	// 经度
	@Column(name = "longitude", precision = 10, scale = 6, nullable = true)
	private BigDecimal longitude; 
	
	// 纬度
	@Column(name = "latitude", precision = 10, scale = 6, nullable = true)
	private BigDecimal latitude;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MUserEntity getUser() {
		return user;
	}

	public void setUser(MUserEntity user) {
		this.user = user;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

}
