package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 用户访问表
* @author LGC
* @date 2018年9月15日
* @version V1.0
*/
@Entity
@Table(name = "e_user_access")
public class UserAccessEntity extends BaseEntity 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5390829184396899069L;
	
	public UserAccessEntity() {
		
	}
	
	public UserAccessEntity(Long id, Long activeId, Long passiveId) {
		this.id = id;
		this.activeId = activeId;
		this.passiveId = passiveId;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "active_id", length = 20, nullable = true)
	private Long activeId;
	
	@Column(name = "passive_id", length = 20, nullable = true)
	private Long passiveId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getPassiveId() {
		return passiveId;
	}

	public void setPassiveId(Long passiveId) {
		this.passiveId = passiveId;
	}
}
