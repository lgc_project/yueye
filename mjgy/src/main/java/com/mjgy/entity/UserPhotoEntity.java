package com.mjgy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* @Description: 用户图库
* @author LGC
* @date 2018年7月17日
* @version V1.0
*/
@Entity
@Table(name = "e_user_photo")
public class UserPhotoEntity implements Serializable {

	private static final long serialVersionUID = 5895292944555801956L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// 图片路径
	@Column(name = "image_path", length = 255, nullable = false)
	private String imagePath;
	
	// 等级（0：普通照片  1：阅后即焚  2：红包照片）
	@Column(name = "level", length = 1, nullable = true)
	private int level;
	
	// 查看红包需要支付的金额
	@Column(name = "check_pay", scale = 2, precision = 10, nullable = true)
	private BigDecimal checkPay;
	
	// 可查看的状态（0：不可看 1：可看）
	@Transient
	private boolean checkStatus;
	
	// 新增时间
	@Column(name = "create_time", length = 10, nullable = true)
	private Date createTime;
	
	// 更新时间
	@Column(name = "update_time", length = 10, nullable = true)
	private Date updateTime;
	
	// 映射用户字段
	@ManyToOne(cascade = CascadeType.REFRESH, optional = false)
	@JsonIgnore
	private MUserEntity user;

	@Column
	private boolean status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public BigDecimal getCheckPay() {
		return checkPay;
	}

	public void setCheckPay(BigDecimal checkPay) {
		this.checkPay = checkPay;
	}

	public boolean getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(boolean checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public MUserEntity getUser() {
		return user;
	}

	public void setUser(MUserEntity user) {
		this.user = user;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
