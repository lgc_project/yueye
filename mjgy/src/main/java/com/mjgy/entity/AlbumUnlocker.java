package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 用户解锁付费相册中间表，解锁有效期7天
* @author quanlq
* @date 2018年10月7日
* @version V1.0
*/
@Entity
@Table(name = "album_unlocker")
public class AlbumUnlocker implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户ID*/
	@Column(name = "user_id", nullable = true)
	private Long userId;
	
	/** 解锁付费相册的人ID*/
	@Column(name = "unlocker_id", nullable = true)
	private Long unlockerId;
	
	/** 类型：0=付费查看，1=会员免费查看（每天3次）*/
	@Column(name = "type", nullable = true)
	private Integer type;
	
	/** 创建时间*/
	@Column(name = "create_time", nullable = true)
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUnlockerId() {
		return unlockerId;
	}

	public void setUnlockerId(Long unlockerId) {
		this.unlockerId = unlockerId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
