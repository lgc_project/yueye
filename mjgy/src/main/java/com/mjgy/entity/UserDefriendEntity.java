package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 用户拉黑表
* @author LGC
* @date 2018年9月18日
* @version V1.0
*/
@Entity
@Table(name = "e_user_defriend")
public class UserDefriendEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5044152912740618028L;

	public UserDefriendEntity() {
	
	}
	
	public UserDefriendEntity(Long id, Long activeId, Long passiveId, boolean defriend) {
		this.id = id;
		this.activeId = activeId;
		this.passiveId = passiveId;
		this.defriend = defriend;
	}
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "active_id", length = 20, nullable = true)
	private Long activeId;
	
	@Column(name = "passive_id", length = 20, nullable = true)
	private Long passiveId;

	@Column(name = "defriend", length = 1, nullable = true)
	private boolean defriend;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getPassiveId() {
		return passiveId;
	}

	public void setPassiveId(Long passiveId) {
		this.passiveId = passiveId;
	}

	public boolean isDefriend() {
		return defriend;
	}

	public void setDefriend(boolean defriend) {
		this.defriend = defriend;
	}
	
	
}
