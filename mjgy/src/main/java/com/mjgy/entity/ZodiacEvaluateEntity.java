package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 星座评价表
* @author LGC
* @date 2018年12月13日
* @version V1.0
*/
@Entity
@Table(name = "e_zodiac_evaluate")
public class ZodiacEvaluateEntity extends BaseEntity {

	private static final long serialVersionUID = 2398710059589293696L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "zodiac_id", length = 20, nullable = true)
	private Long zodiacId;
	
	@Column(name = "user_id", length = 20, nullable = true)
	private Long userId;
	
	@Column(name = "content", length = 255, nullable = true)
	private String content;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getZodiacId() {
		return zodiacId;
	}

	public void setZodiacId(Long zodiacId) {
		this.zodiacId = zodiacId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
