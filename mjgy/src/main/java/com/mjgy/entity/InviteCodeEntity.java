package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
* @Description: 邀请码
* @author LGC
* @date 2018年7月11日
* @version V1.0
*/
@Entity
@Table(name = "e_invitecode")
public class InviteCodeEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3261982010999379117L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// 邀请码
	@Column(name = "invite_code", length = 20, nullable = true)
	private String inviteCode;
	
	// 申请时间
	@Column(name = "application_time", length = 10, nullable = true)
	private Date applicationTime;
	
	// 是否使用
	@Column(name = "is_used", length = 1, nullable = true)
	private boolean isUsed = false;
	
	// 邀请码类型（1：新用户申请（刚注册的用户需要用该邀请码完成注册） 2：老用户申请（帮别人申请的邀请码））
	@Column(name = "type", length = 1, nullable = true)
	private int type = 0;
	
	// CascadeType.ALL：级联
	@ManyToOne(cascade = CascadeType.REFRESH, optional = false)
	@JsonIgnore
	private MUserEntity user;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInviteCode() {
		return inviteCode;
	}
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	public Date getApplicationTime() {
		return applicationTime;
	}
	public void setApplicationTime(Date applicationTime) {
		this.applicationTime = applicationTime;
	}
	public MUserEntity getUser() {
		return user;
	}
	public void setUser(MUserEntity user) {
		this.user = user;
	}
	public boolean isUsed() {
		return isUsed;
	}
	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
