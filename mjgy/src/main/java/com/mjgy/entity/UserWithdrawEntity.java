package com.mjgy.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
@Entity
@Table(name = "e_user_withdraw")
public class UserWithdrawEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户ID*/
	@Column(name = "user_id")
	private Long userId;
	
	/** 提现金额*/
	@Column(name = "amount", scale = 2, precision = 10, nullable = true)
	private BigDecimal amount;
	
	/** 实际转账*/
	@Column(name = "actual", scale = 2, precision = 10, nullable = true)
	private BigDecimal actual;
	
	/** 账户*/
	@Column(name = "account", length = 20, nullable = true)
	private String account;
	
	/** 名字*/
	@Column(name = "name", length = 20, nullable = true)
	private String name;
	
	/** 备注*/
	@Column(name = "memo", nullable = true)
	private String memmo;
	
	/** 申请提现时间*/
	@Column(name = "apply_time", nullable = true)
	private Date applyTime;
	
	/** 状态：0=待处理，1=已执行，2=已打回*/
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getActual() {
		return actual;
	}

	public void setActual(BigDecimal actual) {
		this.actual = actual;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMemmo() {
		return memmo;
	}

	public void setMemmo(String memmo) {
		this.memmo = memmo;
	}

	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	
}
