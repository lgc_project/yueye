package com.mjgy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: token表，记录用户登录状态
* @author LGC
* @date 2018年9月19日
* @version V1.0
*/
@Entity
@Table(name = "e_token")
public class TokenEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5536909639083404136L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "token", length = 36, nullable = true)
	private String token;
	
	@Column(name = "user_id", length = 20, nullable = true)
	private Long userId;
	
	@Column(name = "expire_time", length = 10, nullable = true)
	private Date expireTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
}
