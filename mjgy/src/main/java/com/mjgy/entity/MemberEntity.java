package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 会员实体类
* @author LGC
* @date 2018年9月30日
* @version V1.0
*/
@Entity
@Table(name = "e_member")
public class MemberEntity extends BaseEntity {

	private static final long serialVersionUID = -511061897213688503L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// 用户ID
	@Column(name = "user_id", length = 20, nullable = true)
	private Long userId;
	
	// 充值总天数
	@Column(name = "total_days", length = 5, nullable = true)
	private int totalDays;
	
	// 剩余天数
	@Column(name = "surplus_days", length = 5, nullable = true)
	private int surplusDays;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(int totalDays) {
		this.totalDays = totalDays;
	}

	public int getSurplusDays() {
		return surplusDays;
	}

	public void setSurplusDays(int surplusDays) {
		this.surplusDays = surplusDays;
	}
	
	
}
