package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
* @Description: 消息中心
* @author quanlq
* @date 2018年9月1日
* @version V1.0
*/
@Entity
@Table(name = "e_message")
public class MessageEntity implements Serializable{

	private static final long serialVersionUID = -836156406993776275L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户ID*/
	@Column(name = "user_id", length = 10, nullable = true)
	private Long userId;
	
	/** 消息类型：0=查看申请，1=系统消息，2=收益提醒，3=评价通知*/
	@Column(name = "type", length = 10, nullable = true)
	private String type;
	
	/** 消息发送人ID*/
	@Column(name = "sender_id", nullable = true)
	private Long senderId;

	/** 消息内容*/
	@Column(name = "content", nullable = true)
	private String content;
	
	/** 消息发送时间*/
	@Column(name = "send_time", length = 10, nullable = true)
	private Date sendTime;
	
	/** 是否已阅：0=否，1=是*/
	@Column(name = "is_checked", nullable = true)
	private int isChecked;
	
	/** 是否已通过查看验证：0=已拒绝，1=已通过，2=未处理*/
	@Column(name = "is_allowed", nullable = true, columnDefinition = "int default 2")
	private Integer isAllowed;
	
	/** 申请查看资料发送的照片*/
	@Column(name = "send_image", nullable = true)
	private String sendImage;
	
	/** 照片是否被焚毁：0=否，1=是*/
	@Column(name = "is_burned", nullable = true)
	private Integer isBurned;
	
	/** 消息发送人名称*/
	@Transient
	private String senderName;
	
	/** 头像*/
	@Transient
	private String avatar;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public int getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}

	public Integer getIsAllowed() {
		return isAllowed;
	}

	public void setIsAllowed(Integer isAllowed) {
		this.isAllowed = isAllowed;
	}

	public String getSendImage() {
		return sendImage;
	}

	public void setSendImage(String sendImage) {
		this.sendImage = sendImage;
	}

	public Integer getIsBurned() {
		return isBurned;
	}

	public void setIsBurned(Integer isBurned) {
		this.isBurned = isBurned;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
