package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 查看联系方式白名单
* @author quanlq
* @date 2018年10月7日
* @version V1.0
*/
@Entity
@Table(name = "contact_way_white_list")
public class ContactWayWhiteList implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 用户ID*/
	@Column(name = "user_id", nullable = true)
	private Long userId;
	
	/** 查看联系方式的人ID*/
	@Column(name = "checker_id", nullable = true)
	private Long checkerId;
	
	/** 类型：0=付费查看，1=会员免费查看（每天3次）*/
	@Column(name = "type", nullable = true, columnDefinition = "int default 0")
	private Integer type;
	
	/** 创建时间*/
	@Column(name = "create_time", nullable = true)
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(Long checkerId) {
		this.checkerId = checkerId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	
}
