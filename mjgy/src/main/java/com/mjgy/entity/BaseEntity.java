package com.mjgy.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
* @Description: 实体类基类，包含新增时间，更新时间等字段
* @author Administrator
* @date 2018年9月11日
* @version V1.0
*/
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = -8862810661445843878L;
	
	@Column(name = "create_time", length = 10, nullable = true)
	private Date createTime;
	
	@Column(name = "update_time", length = 10, nullable = true)
	private Date updateTime;
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}
