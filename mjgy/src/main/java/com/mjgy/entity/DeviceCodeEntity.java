package com.mjgy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 设备码实体类，每个用户都有多个设备码
* @author LGC
* @date 2018年10月11日
* @version V1.0
*/
@Entity
@Table(name = "e_device_code")
public class DeviceCodeEntity extends BaseEntity {

	private static final long serialVersionUID = 7955932347123115555L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 用户ID
	 */
	@Column
	private Long userId;
	
	/**
	 * 设备码
	 */
	@Column
	private String deviceCode;
	
	/**
	 * 是否封禁
	 */
	@Column
	private boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDeviceCode() {
		return deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
