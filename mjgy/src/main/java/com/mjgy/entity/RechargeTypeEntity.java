package com.mjgy.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @Description: 会员充值套餐
* @author LGC
* @date 2018年10月12日
* @version V1.0
*/
@Entity
@Table(name = "e_recharge_type")
public class RechargeTypeEntity extends BaseEntity {

	private static final long serialVersionUID = -1673640939950785758L;

	@Id
	@GeneratedValue
	private Long id;
	
	// 充值天数
	@Column
	private int rechargeDays;
	
	// 充值金额
	@Column
	private BigDecimal rechargeAmount;
	
	// 折扣
	@Column
	private double dicount;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getRechargeAmount() {
		return rechargeAmount;
	}

	public void setRechargeAmount(BigDecimal rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}

	public double getDicount() {
		return dicount;
	}

	public void setDicount(double dicount) {
		this.dicount = dicount;
	}

	public int getRechargeDays() {
		return rechargeDays;
	}

	public void setRechargeDays(int rechargeDays) {
		this.rechargeDays = rechargeDays;
	}
	
	
	
}
