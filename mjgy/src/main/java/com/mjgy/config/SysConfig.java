package com.mjgy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
* @Description: 系统参数设置
* @author LGC
* @date 2018年9月10日
* @version V1.0
*/
@Component
@ConfigurationProperties(prefix="sysconfig")
public class SysConfig {

	// 短信验证
	public static String messProduct;
	public static String messDomain;
	public static String messAppKey;
	public static String messAppSecret;
	public static String messSignName;
	public static String messTemplateCode;
	public static String messRegisterCode;
	public static String messBindPhoneCode;
	public static String messUpdatePassword;
	public static String messInviteCode;
	
	public static String appId;
	public static String mchId;
	public static String paternerKey;
	
	public static String alipayAppId;
	public static String alipayMchKey;
	public static String alipayPublicKey;
	
	public static String zodiacKey;
	
	public static String happyKey;
	
	public static String getMessProduct() {
		return messProduct;
	}
	public static void setMessProduct(String messProduct) {
		SysConfig.messProduct = messProduct;
	}
	public static String getMessDomain() {
		return messDomain;
	}
	public static void setMessDomain(String messDomain) {
		SysConfig.messDomain = messDomain;
	}
	public static String getMessAppKey() {
		return messAppKey;
	}
	public static void setMessAppKey(String messAppKey) {
		SysConfig.messAppKey = messAppKey;
	}
	public static String getMessAppSecret() {
		return messAppSecret;
	}
	public static void setMessAppSecret(String messAppSecret) {
		SysConfig.messAppSecret = messAppSecret;
	}
	public static String getMessSignName() {
		return messSignName;
	}
	public static void setMessSignName(String messSignName) {
		SysConfig.messSignName = messSignName;
	}
	public static String getMessTemplateCode() {
		return messTemplateCode;
	}
	public static void setMessTemplateCode(String messTemplateCode) {
		SysConfig.messTemplateCode = messTemplateCode;
	}
	public static String getMessRegisterCode() {
		return messRegisterCode;
	}
	public static void setMessRegisterCode(String messRegisterCode) {
		SysConfig.messRegisterCode = messRegisterCode;
	}
	public static String getMessBindPhoneCode() {
		return messBindPhoneCode;
	}
	public static void setMessBindPhoneCode(String messBindPhoneCode) {
		SysConfig.messBindPhoneCode = messBindPhoneCode;
	}
	public static String getMessUpdatePassword() {
		return messUpdatePassword;
	}
	public static void setMessUpdatePassword(String messUpdatePassword) {
		SysConfig.messUpdatePassword = messUpdatePassword;
	}
	public static String getMessInviteCode() {
		return messInviteCode;
	}
	public static void setMessInviteCode(String messInviteCode) {
		SysConfig.messInviteCode = messInviteCode;
	}
	public static String getAppId() {
		return appId;
	}
	public static void setAppId(String appId) {
		SysConfig.appId = appId;
	}
	public static String getMchId() {
		return mchId;
	}
	public static void setMchId(String mchId) {
		SysConfig.mchId = mchId;
	}
	public static String getPaternerKey() {
		return paternerKey;
	}
	public static void setPaternerKey(String paternerKey) {
		SysConfig.paternerKey = paternerKey;
	}
	public static String getAlipayAppId() {
		return alipayAppId;
	}
	public static void setAlipayAppId(String alipayAppId) {
		SysConfig.alipayAppId = alipayAppId;
	}
	public static String getAlipayMchKey() {
		return alipayMchKey;
	}
	public static void setAlipayMchKey(String alipayMchKey) {
		SysConfig.alipayMchKey = alipayMchKey;
	}
	public static String getAlipayPublicKey() {
		return alipayPublicKey;
	}
	public static void setAlipayPublicKey(String alipayPublicKey) {
		SysConfig.alipayPublicKey = alipayPublicKey;
	}
	public static String getZodiacKey() {
		return zodiacKey;
	}
	public static void setZodiacKey(String zodiacKey) {
		SysConfig.zodiacKey = zodiacKey;
	}
	public static String getHappyKey() {
		return happyKey;
	}
	public static void setHappyKey(String happyKey) {
		SysConfig.happyKey = happyKey;
	}
	
}
