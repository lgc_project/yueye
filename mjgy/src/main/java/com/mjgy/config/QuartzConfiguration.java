package com.mjgy.config;

import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.mjgy.service.JobService;

/**
* @Description: 定时任务配置类
* @author LGC
* @date 2018年9月30日
* @version V1.0
*/
@Configuration
public class QuartzConfiguration {

	@Bean(name = "memberRunDay")
	public MethodInvokingJobDetailFactoryBean firstJobDetail(JobService jobServiec) {
		MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
		
		jobDetail.setConcurrent(false);
		jobDetail.setName("memberRunDayJob");
		
		jobDetail.setTargetObject(jobServiec);
		jobDetail.setTargetMethod("memberRunDay");
		return jobDetail;
	}
	
	/**
	 * 每天00:00执行
	 * 
	 * @param firstJobDetail
	 * @return
	 */
	@Bean(name = "firstTrigger")
	public CronTriggerFactoryBean firstTrigger(@Qualifier("memberRunDay") MethodInvokingJobDetailFactoryBean firstJobDetail) {
		CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
		trigger.setJobDetail(firstJobDetail.getObject());
		trigger.setCronExpression("0 0 0 * * ?"); // 初始化cron表达式
//		trigger.setCronExpression("0 * * * * ?");
		return trigger;
	}
	
	@Bean(name = "scheduler")
	public SchedulerFactoryBean schedulerFactory(Trigger firstTrigger) {
		SchedulerFactoryBean bean = new SchedulerFactoryBean();
		// 延时启动，应用启动1秒后
		bean.setStartupDelay(1);
		// 注册触发器
		bean.setTriggers(firstTrigger);
		
		return bean;
		
	}
	
}
