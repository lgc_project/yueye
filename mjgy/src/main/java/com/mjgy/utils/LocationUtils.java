package com.mjgy.utils;
/**
* @Description: 根据经纬度获取位置工具类
* @author LGC
* @date 2018年9月12日
* @version V1.0
*/
public class LocationUtils {

	private static final double EARTH_RADIUS = 6378.137; // 地球半径
	
	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}
	
	/**
	 * 根据两个点的经纬度计算距离（公里）
	 * 
	 * @param lon1 经度1
	 * @param lat1 纬度1
	 * @param lon2 经度2
	 * @param lat2 纬度2
	 * @return
	 */
	public static double getDistance(double lon1, double lat1, double lon2, double lat2) {
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double lonD = rad(lon1) - rad(lon2);
		
		double d = EARTH_RADIUS * Math.acos(Math.cos(radLat1) * 
				Math.cos(radLat2) * Math.cos(lonD) + Math.sin(radLat1) * Math.sin(radLat2));
		return d;
	}
	
	
}
