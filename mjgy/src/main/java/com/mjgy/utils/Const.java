package com.mjgy.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
* @Description: TODO
* @author LGC
* @date 2018年9月10日
* @version V1.0
*/
public class Const {

	// 返回的成功码
	public static final int SUCCESS = 200;
	
	// 返回的错误码
	public static final int FAILED = 201;
	
	// 验证码有效时间为 15 分钟（15 * 60 * 1000）
	public static final Long VERFICODETIME = 900000L;
	
	// token值有效时间为1个星期（7 * 24 * 60 * 60 * 1000）
	public static final Long EXPIRE_TIEM = 604800000L;
	
	public synchronized static String getOutTradeNo() {
    	Date date = new Date();
    	DateFormat format = new SimpleDateFormat("yyyyMMdd");
    	String time = format.format(date);
    	int hashCodeV = UUID.randomUUID().toString().hashCode();
    	if(hashCodeV < 0) {
    		hashCodeV = -hashCodeV;
    	}
    	
    	// 0 代表前面补充0
        // 6 代表长度为6
        // d 代表参数为正数型
    	return time + String.format("%06d", hashCodeV);
    	
    }
}
