package com.mjgy.controller;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.R;
import com.mjgy.service.UserPhotoService;

/**
* @Description: 用户图片
* @author LGC
* @date 2018年7月31日
* @version V1.0
*/
@RestController
@RequestMapping("/userPhoto")
public class UserPhotoController {

	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private UserPhotoService userPhotoService;
	
	/**
	 * 上传用户图库
	 * 
	 * @param userId
	 * @param level 图片的等级（0：普通 1：阅后即焚 2：红包）
	 * @param checkPay
	 * @param imagePath
	 * @return
	 */
	@RequestMapping("/saveUserPhoto")
	public R saveUserPhoto(Long userId, int level, BigDecimal checkPay, String imagePath) {
		return userPhotoService.saveUserPhoto(userId, level, checkPay, imagePath);
	}
	
	/**
	 * 获取用户图库
	 * 
	 * @param userId 被查看的用户
	 * @param viewerId 查看的用户
	 * @return
	 */
	@RequestMapping("/getUserPhoto")
	public R getUserPhoto(Long userId, Long viewerId) {
		return userPhotoService.getUserPhoto(userId, viewerId);
	}
	
	/**
	 * 设置图片为阅后即焚图片
	 * 
	 * @param photoId
	 * @return
	 */
	@RequestMapping("/updateUserPhoto")
	public R updateUserPhoto(Long photoId) {
		return userPhotoService.updateUserPhoto(photoId);
	}
	
	/**
	 * 删除用户图片
	 * 
	 * @param photoId
	 * @return
	 */
	@RequestMapping("/delete")
	public R deleteUserPhoto(Long photoId) {
		return userPhotoService.deleteUserPhoto(photoId);
	}
	
	/**
	 * 编辑图片状态（封禁/解封）
	 * 
	 * @param photoId
	 * @param status
	 * @return
	 */
	@RequestMapping("/editPhotoStatus/{photoId}/{status}")
	public R editPhotoStatus(@PathVariable Long photoId, @PathVariable boolean status) {
		return userPhotoService.editPhotoStatus(photoId, status);
	}

}
