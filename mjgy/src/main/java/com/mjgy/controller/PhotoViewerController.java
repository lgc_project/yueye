package com.mjgy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.R;
import com.mjgy.entity.PhotoViewerEntity;
import com.mjgy.service.PhotoViewerService;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
@RestController
@RequestMapping("/photoViewer")
public class PhotoViewerController {
	
	@Autowired
	private PhotoViewerService photoViewerService;
	
	/**
	 * 保存用户查看图片的关系
	 * 
	 * @param photoViewerEntity
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R save(PhotoViewerEntity photoViewerEntity){
		if(photoViewerEntity == null){
			return R.error(202, "入参不能为空");
		}
		if(photoViewerEntity.getType() == 2){
			return R.error(202, "红包图片需要付费，不适用本接口");
		}
		photoViewerEntity.setType(1);
		photoViewerService.save(photoViewerEntity);
		
		return R.success();
	}
	
	/**
	 * 一键恢复阅后即焚
	 * 
	 * @param userId	用户ID
	 */
	@RequestMapping(value = "/recoveryPhoto/{userId}", method = RequestMethod.DELETE)
	public R deleteByUserId(@PathVariable Long userId){
		photoViewerService.deleteByUserIdAndType(userId, 1);	//type=1表示阅后即焚图片
		return R.success();
	}
	
	/**
	 * 统计有多少人焚毁了用户的照片
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/countViewerByUserId/{userId}")
	public R countViewerByUserId(@PathVariable Long userId){
		Integer count = photoViewerService.countViewerByUserId(userId);
		return R.success().put("data", count);
	}

}
