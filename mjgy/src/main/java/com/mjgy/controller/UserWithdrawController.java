package com.mjgy.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.util.StringUtils;
import com.common.utils.R;
import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.UserWithdrawEntity;
import com.mjgy.repository.MUserRepository;
import com.mjgy.repository.UserWithdrawRepository;
import com.mjgy.service.UserWithdrawService;
import com.modules.sys.service.SysConfigService;

/**
* @Description: TODO
* @author quanlq
* @date 2018年10月1日
* @version V1.0
*/
@RestController
@RequestMapping("/userWithdraw")
public class UserWithdrawController {

	@Autowired
	private UserWithdrawService userWithdrawService;
	@Autowired
	private UserWithdrawRepository userWithdrawRepository;
	@Autowired
	private MUserRepository mUserRepository;
	@Autowired
	private SysConfigService sysConfigService;
	
	/**
	 * 用户提现
	 * @param userWithdrawEntity
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R save(UserWithdrawEntity userWithdrawEntity) {
		if(userWithdrawEntity == null){
			return R.error(202, "入参不能为空");
		}
		if(userWithdrawEntity.getUserId() == null){
			return R.error(202, "用户ID不能为空");
		}
		
		MUserEntity mUserEntity = mUserRepository.findOne(userWithdrawEntity.getUserId());
		if(mUserEntity == null){
			return R.error(202, "该用户不存在");
		}
		
		BigDecimal amount = userWithdrawEntity.getAmount();
		if(amount == null){
			return R.error(202, "提现金额不能为空");
		}
		if(amount.compareTo(mUserEntity.getWallet() == null? BigDecimal.ZERO : mUserEntity.getWallet()) > 0){
			return R.error(202, "提现金额不能大于钱包金额");
		}
		
		userWithdrawEntity.setApplyTime(new Date());	//设置申请提现时间
		String charge = sysConfigService.getValue("WITHDRAW_CHARGE");
		if(StringUtils.isEmpty(charge)){
			charge = "0.1";	//	手续费默认10%
		}
		userWithdrawEntity.setActual(amount.multiply(new BigDecimal(1).subtract(new BigDecimal(charge))));
		userWithdrawEntity.setStatus(0);	//将提现记录的状态设为待处理
		
		userWithdrawService.save(userWithdrawEntity);
		return R.success();
	}
	
	
	//------------------------------------  管理后台  -----------------------------------------------//
	
	/**
	 * 提现列表
	 * @param status	状态：0=待处理，1=已执行，2=已打回
	 * @param city	所在城市
	 * @param nickname	昵称
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	@RequestMapping("/findListByStatus")
	public R findListByStatus(Integer status, Long id, String city, String nickname, 
			String startTime, String endTime, Integer page, Integer size){
		if(page == null){
			page = 1;
		}
		if(size == null){
			size = 20;
		}
		if(city == null){
			city = "";
		}
		if(nickname == null){
			nickname = "";
		}
		if(startTime != null && endTime != null && startTime.compareTo(endTime) > 0){
			return R.error(202, "开始时间不能大于结束时间");
		}
		List<Map<String, Object>> list = userWithdrawService.findListByStatus(status, id, city, nickname, 
				startTime, endTime, page, size);
		Long totalCount = userWithdrawRepository.countByStatusAndCityAndNickname(status, id, city, nickname, 
				startTime, endTime);
		Long totalPage = totalCount / size + (totalCount % size == 0?0:1);
		
		Map<String, Object> data = new HashMap<>();
		data.put("list", list);
		data.put("currPage", page);
		data.put("pageSize", size);
		data.put("totalCount", totalCount);
		data.put("totalPage", totalPage);
		return R.success().put("data", data);
	}
	
	/**
	 * 处理提现
	 * @param id 提现ID
	 * @param status 状态
	 * @return
	 */
	@RequestMapping("/dealUserWithdraw")
	public R dealUserWithdraw(Long id, Integer status){
		if(id == null){
			return R.error(202, "ID不能为空");
		}
		if(status == null){
			return R.error(202, "状态不能为空");
		}
		
		return userWithdrawService.dealUserWithdraw(id, status);
	}
	
}
