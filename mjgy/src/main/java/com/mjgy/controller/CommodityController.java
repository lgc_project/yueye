package com.mjgy.controller;

import com.common.utils.R;
import com.mjgy.service.CommodityService;
import com.mjgy.service.DealService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @author lguochao
 * @date 2019年1月9日
 * @version V1.0
 */
@RestController
@RequestMapping("/commodity")
public class CommodityController {

	@Autowired
	private CommodityService commodityService;

	@Autowired
	private DealService dealService;
	
	@RequestMapping("/getAll")
	public R getAllCommodity(Integer page, Integer pageSize) {
		return commodityService.getAllCommodity(page, pageSize);
	}

	/**
	* @description: 保存订单
	* @param userId 用户ID
	* @param commodityId 商品ID
	* @param receiver 接收人
	* @param address 地址
	* @param phone 电话
	* @return: 
	*/
	@RequestMapping("/saveDeal")
	public R saveDeal(Long userId, Long commodityId, String receiver, String address, String phone) {
		return dealService.saveDeal(userId, commodityId, receiver, address, phone);
	}
	
	/**
	* @description: 获取用户订单列表
	* @param userId 用户ID
	* @param page
	* @param pageSize  
	* @return: 
	*/
	@RequestMapping("/getDealList")
	public R getDealList(Long userId, Integer page, Integer pageSize) {
		return dealService.getDealList(userId, page, pageSize);
	}
}
