package com.mjgy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.R;
import com.mjgy.service.ZodiacService;

/**
* @Description: 星座接口
* @author LGC
* @date 2018年12月13日
* @version V1.0
*/
@RestController
@RequestMapping(value = "/zodiac")
public class ZodiacController {

	@Autowired
	private ZodiacService zodiacService;
	
	/**
	 * 星座评价
	 * 
	 * @param zodiacId 星座ID
	 * @param userId 用户ID
	 * @param content 评价内容
	 * @return
	 */
	@RequestMapping("/evaluate")
	public R evaluate(Long zodiacId, Long userId, String content) {
		return zodiacService.evaluate(zodiacId, userId, content);
	}
	
	/**
	 * 星座评价列表
	 * 
	 * @param zodiacId 星座ID
	 * @param page 当前页（可为空）
	 * @param pageSize 一页总数 （可为空）
	 * @return
	 */
	@RequestMapping("/evaluateList")
	public R evaluateList(Long zodiacId, Integer page, Integer pageSize) {
		return zodiacService.evaluateList(zodiacId, page, pageSize);
	}
	
	/**
	 * 获取星座运势
	 * 
	 * @param name 星座名称
	 * @param type 运势类型，如：today,tomorrow,week,nextweek,month,year
	 * @return
	 */
	@RequestMapping("/getZodiacFortune")
	public R getZodiacFortune(String name, String type) {
		return zodiacService.getZodiacFortune(name, type);
	}
	
	/**
	 * 笑话大全
	 * 
	 * @param page 当前页，默认1，可为空
	 * @param pageSize 每次返回条数，默认1，可为空
	 * @param time 时间戳（10位）
	 * @return
	 */
	@RequestMapping("/getJuhe")
	public R getJuhe(Integer page, Integer pageSize, String time) {
		return zodiacService.getJuhe(page, pageSize, time);
	}
}
