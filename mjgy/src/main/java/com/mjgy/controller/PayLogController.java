package com.mjgy.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.R;
import com.mjgy.entity.PayLog;
import com.mjgy.repository.PayLogRepository;
import com.mjgy.service.PayLogService;

/**
* @Description: 支付记录
* @author quanlq
* @date 2018年10月9日
* @version V1.0
*/
@RestController
@RequestMapping("/payLog")
public class PayLogController {

	@Autowired
	private PayLogService payLogService;
	@Autowired
	private PayLogRepository payLogRepository;
	
	/**
	 * 生成一条支付记录
	 * @param payLog 支付记录实体
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R save(PayLog payLog){
		if(payLog != null){
			payLog.setCreateTime(new Date());
		}
		payLogService.save(payLog);
		return R.success();
	}
	
	//----------------------------------  管理后台接口  -----------------------------------//
	
	/**
	 * 收入记录统计数据
	 * @param startDate	开始日期
	 * @param endDate	结束日期
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	@RequestMapping("/statistic")
	public R statistic(String startDate, String endDate, Integer page, Integer size){
		if(page == null){
			page = 1;
		}
		if(size == null){
			size = 20;
		}
		List<Map<String, Object>> list = payLogService.statistic(startDate, endDate, page, size);
		Long totalCount = payLogRepository.countStatistic(startDate, endDate);
		Long totalPage = totalCount / size + (totalCount % size == 0?0:1);
		
		Map<String, Object> data = new HashMap<>();
		data.put("list", list);
		data.put("currPage", page);
		data.put("pageSize", size);
		data.put("totalCount", totalCount);
		data.put("totalPage", totalPage);
		return R.success().put("data", data);
	}
	
	/**
	 * 根据城市统计收入记录
	 * @param date	日期
	 * @param page	页码
	 * @param size	每页条数
	 * @return
	 */
	@RequestMapping("/statisticByCity")
	public R statisticByCity(String date, Integer page, Integer size) {
		if(page == null){
			page = 1;
		}
		if(size == null){
			size = 20;
		}
		List<Map<String, Object>> list = payLogService.statisticByCity(date, page, size);
		Long totalCount = payLogRepository.countStatisticByCity(date);
		Long totalPage = totalCount / size + (totalCount % size == 0?0:1);
		
		Map<String, Object> data = new HashMap<>();
		data.put("list", list);
		data.put("currPage", page);
		data.put("pageSize", size);
		data.put("totalCount", totalCount);
		data.put("totalPage", totalPage);
		return R.success().put("data", data);
	}
	
}
