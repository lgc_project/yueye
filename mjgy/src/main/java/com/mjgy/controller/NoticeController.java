package com.mjgy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.R;
import com.mjgy.service.NoticeService;

/**
* @Description: 公告接口（包括客服信息）
* @author LGC
* @date 2018年10月2日
* @version V1.0
*/
@RestController
@RequestMapping("/notice")
public class NoticeController {
	
	@Autowired
	private NoticeService noticeService;
	
	
	/**
	 * 保存公告信息
	 * 
	 * @param title
	 * @param content
	 * @return
	 */
	@RequestMapping("/saveNotice")
	public R saveNotice(String title, String content) {
		return noticeService.saveNotice(title, content);
	}
	
	/**
	 * 修改公告信息
	 * 
	 * @param noticeId
	 * @param title
	 * @param content
	 * @return
	 */
	@RequestMapping("/updateNotice")
	public R updateNotice(Long noticeId, String title, String content) {
		return noticeService.updateNotice(noticeId, title, content);
	}
	
	/**
	 * 获取公告
	 * 
	 * @return
	 */
	@RequestMapping("/getNotice")
	public R getNotice() {
		return noticeService.getNotice();
	}
	
}
