package com.mjgy.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aliyuncs.utils.StringUtils;
import com.common.utils.R;
import com.mjgy.entity.CheckWhiteList;
import com.mjgy.entity.MUserEntity;
import com.mjgy.entity.MessageEntity;
import com.mjgy.repository.CheckWhiteListRepository;
import com.mjgy.repository.MUserRepository;
import com.mjgy.repository.MessageRepository;
import com.mjgy.service.MessageService;

/**
* @Description: 消息中心Controller
* @author quanlq
* @date 2018年9月1日
* @version V1.0
*/
@RestController
@RequestMapping("/message")
public class MessageController {

	@Autowired
	private MessageService messageService;
	@Autowired
	private CheckWhiteListRepository checkWhiteListRepository;
	@Autowired
	private MUserRepository mUserRepository;
	@Autowired
	private MessageRepository messageRepository;
	
	/**
	 * 获取我的未读消息总数
	 */
	@RequestMapping("/getTotalUncheckedMsgNum")
	public R getTotalUncheckedMsgNum(Long userId){
		Integer total = messageService.countByUserIdAndIsChecked(userId, 0);
		return R.success().put("data", total);
	}
	
	/**
	 * 获取最新消息及对应类型的未读消息数
	 * 
	 * @return
	 */
	@RequestMapping("/findNewestMessages/{userId}")
	public R findNewestMessages(@PathVariable Long userId){
		Map<String, Object> map = messageService.findNewestMessages(userId);
		return R.success().put("data", map);
	}
	
	/**
	 * 根据消息类型获取消息列表
	 * 
	 * @param userId 用户ID
	 * @param type 消息类型：0=查看申请，1=系统消息，2=收益提醒，3=评价通知
	 * @param pageable 分页
	 * @return
	 */
	@RequestMapping("/findMsgByType/{type}")
	public R findMsgByType(@PathVariable String type, Long userId, Integer page, Integer size){
		if(page == null){
			page = 1;
		}
		if(size == null){
			size = 20;
		}
		List<Map<String, Object>> list = messageService.findByUserIdAndType(userId, type, page, size);
		return R.success().put("data", list);
	}
	
	/**
	 * 发送一条消息
	 * @param messageEntity
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public R save(MessageEntity messageEntity){
		if(messageEntity != null){
			messageEntity.setSendTime(new Date());
			messageEntity.setIsChecked(0);	//0表示该消息未阅读
			messageEntity.setIsAllowed(2);	//2表示未处理是否通过查看验证的消息
			messageEntity.setIsBurned(0);	//0表示照片未焚毁
		}
		messageService.save(messageEntity);
		return R.success();
	}
	
	/**
	 * 允许/拒绝查看申请
	 * @param userId	用户ID
	 * @param checkerId	查看人ID
	 * @param messageId	消息ID
	 * @param isAllowed	是否允许查看
	 * @return
	 */
	@RequestMapping(value = "/dealCheckApply", method = RequestMethod.POST)
	public R dealCheckApply(Long userId, Long checkerId, Long messageId, Integer isAllowed){
		if(isAllowed == 0){
			messageService.updateIsAllowedById(isAllowed, messageId);
		}else if (isAllowed == 1){
			messageService.updateIsAllowedById(isAllowed, messageId);
			
			CheckWhiteList checkWhiteList = new CheckWhiteList();
			checkWhiteList.setCheckerId(checkerId);
			checkWhiteList.setUserId(userId);
			checkWhiteList.setCreateTime(new Date());
			checkWhiteListRepository.save(checkWhiteList);
		}
		
		//发送消息
		MUserEntity mUserEntity = mUserRepository.findOne(userId);
		MessageEntity messageEntity = new MessageEntity();
		messageEntity.setIsChecked(0);
		messageEntity.setType("0");	//查看申请
		messageEntity.setSenderId(userId);
		messageEntity.setUserId(checkerId);
		messageEntity.setSendTime(new Date());
		messageEntity.setContent(mUserEntity.getNickname() + (isAllowed==0?" 拒绝":" 同意") + "了你的查看请求！");
		messageService.save(messageEntity);
		return R.success();
	}
	
	/**
	 * 焚毁用户申请查看资料发送的照片
	 * @param messageId	消息ID
	 * @return
	 */
	@RequestMapping(value = "/burnSendImage/{messageId}", method = RequestMethod.PUT)
	public R burnSendImage(@PathVariable Long messageId){
		messageService.updateIsBurnedById(messageId);
		return R.success();
	}
	
	
	//-------------------------------  管理后台相关  -------------------------------------//
	
	/**
	 * 发送系统消息
	 * @param content	消息内容
	 * @return
	 */
	@RequestMapping(value = "/sendSysMsgToAll")
	public R sendSysMsgToAll(String content){
		if(StringUtils.isEmpty(content)){
			return R.error(202, "消息内容不能为空");
		}
		
		List<MessageEntity> list = new ArrayList<>();
		List<MUserEntity> userList = mUserRepository.findAll();
		if(CollectionUtils.isEmpty(userList)){
			return R.error(202, "找不到用户");
		}
		for(MUserEntity user : userList){
			MessageEntity messageEntity = new MessageEntity();
			messageEntity.setContent(content);
			messageEntity.setUserId(user.getId());
			messageEntity.setSendTime(new Date());
			messageEntity.setType("1"); //系统消息
			messageEntity.setIsChecked(0);
			list.add(messageEntity);
		}
		messageRepository.save(list);
		return R.success();
	}
	
}
