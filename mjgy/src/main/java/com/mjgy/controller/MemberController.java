package com.mjgy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.R;
import com.mjgy.service.MemberService;

/**
* @Description: 会员相关接口
* @author LGC
* @date 2018年10月13日
* @version V1.0
*/
@RestController
@RequestMapping("/member")
public class MemberController {

	@Autowired
	private MemberService memberService;
	
	@RequestMapping("/rechargeTypeList")
	public R rechargeTypeList() {
		return memberService.rechargeTypeList();
	}
	
	@RequestMapping("/editRechargeType")
	public R editRechageType(@RequestParam Map<String, Object> params) {
		return memberService.editRechargeType(params);
	}
}
