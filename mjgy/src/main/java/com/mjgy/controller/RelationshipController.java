package com.mjgy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.PageUtils;
import com.common.utils.Query;
import com.common.utils.R;
import com.mjgy.service.RelationshipService;

/**
* @Description: 用户关系接口
* @author LGC
* @date 2018年9月14日
* @version V1.0
*/
@RestController
@RequestMapping(value = "/relationship")
public class RelationshipController {

	@Autowired
	private RelationshipService rs;
	
	/**
	 * 关注
	 * 
	 * @param activeId
	 * @param passiveId
	 * @param follow
	 * @return
	 */
	@RequestMapping("/follow")
	public R follow(Long activeId, Long passiveId, boolean follow) {
		return rs.follow(activeId, passiveId, follow);
	}
	
	/**
	 * 关注列表
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping("/followList")
	public R followList(Long userId) {
		return rs.followList(userId);
	}
	
	/**
	 * 拉黑
	 * 
	 * @param activeId
	 * @param passiveId
	 * @param defriend
	 * @return
	 */
	@RequestMapping("/defriend")
	public R defriend(Long activeId, Long passiveId, boolean defriend) {
		return rs.defriend(activeId, passiveId, defriend);
	}
	
	/**
	 * 拉黑列表
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping("/defriendList")
	public R defriendList(Long userId) {
		return rs.defriendList(userId);
	}
	
	/**
	 * 访问
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	@RequestMapping("/access")
	public R access(Long activeId, Long passiveId) {
		return rs.access(activeId, passiveId);
	}
	
	/**
	 * 历史访客
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping("/accessList")
	public R accessList(Long userId) {
		return rs.accessList(userId);
	}
	
	/**
	 * 评价
	 * 
	 * @param activeId
	 * @param passiveId
	 * @param content
	 * @return
	 */
	@RequestMapping("/evaluate")
	public R evaluate(Long activeId, Long passiveId, String content) {
		return rs.evaluate(activeId, passiveId, content);
	}
	
	/**
	 * 获取个人评价
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping("/evaluateList")
	public R evaluateList(Long userId) {
		return rs.evaluateList(userId);
	}
	
	/**
	 * 获取评价别人的内容
	 * 
	 * @param activeId
	 * @param passiveId
	 * @return
	 */
	@RequestMapping("/evaluateContent")
	public R getEvaluateContent(Long activeId, Long passiveId) {
		return rs.evaluateContent(activeId, passiveId);
	}
	
	//------------------------------  管理后台接口  ---------------------------------//
	
	@RequestMapping("/evaluatePage")
	public R queryEvaluatePage(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		PageUtils pageUtils = rs.queryEvaluatePage(query);
		return R.ok().put("page", pageUtils);
	}
}
