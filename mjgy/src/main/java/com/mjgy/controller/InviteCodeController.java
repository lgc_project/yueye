package com.mjgy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.common.utils.PageUtils;
import com.common.utils.Query;
import com.common.utils.R;
import com.mjgy.entity.InviteCodeEntity;
import com.mjgy.service.InviteCodeService;

/**
* @Description: 邀请码相关接口
* @author LGC
* @date 2018年7月12日
* @version V1.0
*/
@RestController
@RequestMapping("/inviteCode")
public class InviteCodeController {

	@Autowired
	private InviteCodeService inviteCodeService;

	/**
	 * 邀请码列表
	 * 
	 * @param params（page：第几页 limit：一页的限制数 order：排序方式 type:类别）
	 * @return
	 */
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		PageUtils pageUtil = inviteCodeService.queryPage(query);
		return R.ok().put("page", pageUtil);
	}
	
	/**
	 * 生成邀请码
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/buildCode/{id}")
	public R buildCode(@PathVariable Long id) {
		return  inviteCodeService.buildCode(id);
	}
	
	/**
	 * 申请邀请码
	 * 
	 * @param userId
	 * @param type
	 * @return
	 */
	@RequestMapping("/applyCode")
	public R applyCode(Long userId, int type) {
		return inviteCodeService.applyCode(userId, type);
	}
	
	/**
	 * 激活邀请码
	 * 
	 * @param userId
	 * @param inviteCode
	 * @return
	 */
	@RequestMapping("/activateCode")
	public R activateCode(Long userId, String inviteCode) {
		return inviteCodeService.activateCode(userId, inviteCode);
	}
}
