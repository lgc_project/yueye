package com.mjgy.timedtask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.mjgy.repository.AlbumUnlockerRepository;
import com.mjgy.repository.CheckWhiteListRepository;
import com.mjgy.repository.ContactWayWhiteListRepository;

/**
* @Description: 定时器
* @author quanlq
* @date 2018年10月6日
* @version V1.0
*/
@Configuration
@EnableScheduling // 启用定时任务
public class ScheduledTasks {
	
	@Autowired
	private CheckWhiteListRepository checkWhiteListRepository;
	@Autowired
	private AlbumUnlockerRepository albumUnlockerRepository;
	@Autowired
	private ContactWayWhiteListRepository contactWayWhiteListRepository;
	
	/**
	 * 删除查看权限的过期用户关系（超过15天），每5分钟执行一次
	 * @throws Exception
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
	public void deleteOverdueChecker() throws Exception {
		checkWhiteListRepository.deleteOverdueChecker();
	}
	
	/**
	 * 删除解锁付费相册的过期用户关系（超过7天），每5分钟执行一次
	 * @throws Exception
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
	public void deleteOverdueUnlocker() throws Exception {
		albumUnlockerRepository.deleteOverdueUnlocker();
	}
	
	/**
	 * 删除查看联系方式白名单的过期用户关系（超过7天），每5分钟执行一次
	 * @throws Exception
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
	public void deleteOverdueContactWayChecker() throws Exception {
		contactWayWhiteListRepository.deleteOverdueChecker();
	}
	
	/**
	 * 每天凌晨0点清空会员免费查看联系方式的次数
	 * @throws Exception
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	public void deleteContactWayWhiteListByType() throws Exception {
		contactWayWhiteListRepository.deleteByType(1);	//type=1表示会员免费查看联系
	}
	
	/**
	 * 每天凌晨0点清空会员免费查看付费相册的次数
	 * @throws Exception
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	public void deleteAlbumUnlockerByType() throws Exception {
		albumUnlockerRepository.deleteByType(1);	//type=1表示会员免费查看付费相册
	}

}
