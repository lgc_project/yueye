package com.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.DispatcherType;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import com.common.xss.XssFilter;
import com.mjgy.filter.TokenFilter;

/**
 * Filter配置
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-04-21 21:56
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean shiroFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new DelegatingFilterProxy("shiroFilter"));
        //该值缺省为false，表示生命周期由SpringApplicationContext管理，设置为true则表示由ServletContainer管理
        registration.addInitParameter("targetFilterLifecycle", "true");
        registration.setEnabled(true);
        registration.setOrder(Integer.MAX_VALUE - 1);
        registration.addUrlPatterns("/*");
        return registration;
    }

    @Bean
    public FilterRegistrationBean xssFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new XssFilter());
        registration.addUrlPatterns("/*");
        registration.setName("xssFilter");
        registration.setOrder(Integer.MAX_VALUE);
        return registration;
    }
    
    @Bean
    public FilterRegistrationBean tokenFilterRegistration() {
    	FilterRegistrationBean registration = new FilterRegistrationBean();
    	registration.setFilter(new TokenFilter());
    	registration.setName("tokenFilter");
    	registration.setOrder(Integer.MAX_VALUE - 2);
    	// 配置需要拦截的url
    	List<String> list = new ArrayList<String>();
    	//广播
    	list.add("/broadCast/saveBroadCast");
    	list.add("/broadCast/findList");
    	list.add("/broadCast/like");
    	list.add("/broadCast/signUp");
//    	list.add("/broadCast/findSignerList");
    	list.add("/broadCast/deleteMyBroadCast/*");
    	list.add("/broadCast/findByUserId");
    	list.add("/broadCast/dateSomeone");
    	list.add("/broadCast/uncheckedSignerNum");
    	list.add("/broadCast/isDated");
        
        //公共
//    	list.add("/common/sendVerfiCode");
    	list.add("/common/uploadPhoto");
        
        //邀请码
//    	list.add("/inviteCode/list");
//    	list.add("/inviteCode/buildCode/*");
    	list.add("/inviteCode/applyCode");
    	list.add("/inviteCode/activateCode");
        
        //会员
//    	list.add("/member/rechargeTypeList");
//    	list.add("/member/editRechargeType");
        
        //消息
    	list.add("/message/getTotalUncheckedMsgNum");
    	list.add("/message/findNewestMessages/*");
    	list.add("/message/findMsgByType/*");
    	list.add("/message/save/*");
    	list.add("/message/dealCheckApply");
    	list.add("/message/burnSendImage/*");
        
        //用户
    	list.add("/user/updateUserSex");
    	list.add("/user/forgetPassword");
    	list.add("/user/uploadAvatar");
    	list.add("/user/updateUserInfo");
    	list.add("/user/bindPhone");
    	list.add("/user/updatePassword");
    	list.add("/user/getAllUserInfo");
    	list.add("/user/getNearbyUserInfo");
    	list.add("/user/searchUserInfo");
    	list.add("/user/getUserInfo");
    	list.add("/user/updateUserLocation");
    	list.add("/user/hasCheckPermission");
    	list.add("/user/updateCheckLimit");
    	list.add("/user/updateAlbumAmount");
    	list.add("/user/hasAlbumPermission");
    	list.add("/user/hasContactWayPermission");
    	list.add("/user/checkContactWayNum");
    	list.add("/user/signContactWay");
    	list.add("/user/checkAlbumNum");
    	list.add("/user/signAlbum");
    	list.add("/user/identity");
    	list.add("/user/report");
        
        //公告
    	list.add("/notice/saveNotice");
    	list.add("/notice/updateNotice");
    	list.add("/notice/getNotice");
        
        //支付
    	list.add("/pay/getWxPayOrder");
    	list.add("/pay/getAlipayOrder");
        
        //支付记录
    	list.add("/payLog/save");
        
        //图片查看
    	list.add("/photoViewer/save");
    	list.add("/photoViewer/recoveryPhoto/*");
    	list.add("/photoViewer/countViewerByUserId/*");
        
        //用户关系
    	list.add("/relationship/follow");
    	list.add("/relationship/followList");
    	list.add("/relationship/defriend");
    	list.add("/relationship/defriendList");
    	list.add("/relationship/access");
    	list.add("/relationship/accessList");
    	list.add("/relationship/evaluate");
//    	list.add("/relationship/evaluateList");
    	list.add("/relationship/evaluateContent");
        
        //用户图片
    	list.add("/userPhoto/saveUserPhoto");
    	list.add("/userPhoto/getUserPhoto");
    	list.add("/userPhoto/updateUserPhoto");
    	list.add("/userPhoto/delete");
        
        //提现
    	list.add("/userWithdraw/save");
    	registration.setUrlPatterns(list);
    	return registration;
    }
}
