$(function() {
	$("#jqGrid").jqGrid({
		url: baseURL + 'inviteCode/list',
		datatype: "json",
		colModel: [
			{label: '邀请码ID', name: 'id', width: 40},
			{label: '用户账号', name: 'user.username', width: 40},
			{label: '邀请码', name: 'inviteCode', width: 40},
			{label: '申请时间', name: 'applicationTime', width: 45},
			{label: "操作", name: 'edit', width:20}
		],
		viewrecords: true,
		height: 385,
		rowNum: 10,
		rowList: [10, 30, 50],
		rownumbers: true,
		rownumWidth: 25,
		autowidth: true,
		multiselect: true,
		pager: "#jqGridPager",
		jsonReader: {
			root: "page.list",
			page: "page.currPage",
			total: "page.totalPage",
			records: "page.totalCount"
		},
		prmNames: {
			page:"page",
			rows:"limit",
			order:"order"
		},
		gridComplete: function() {
			// 隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        	var ids = $("#jqGrid").jqGrid('getDataIDs');
        	for(var i = 0; i < ids.length; i++) {
        		var id = ids[i];
        		var buildCode = "<a href='#' style='color:#f60' onclick = 'buildCode(" + id + ")'>生成邀请码</a>";
        		$("#jqGrid").jqGrid('setRowData', id, {edit:buildCode });
        	}
		}
	});
});

var vm = new Vue({
	el: '#rrapp',
	data: {
		q: {
			inviteCode: null
		},
		showList: true
	},
	methods: {
		query: function() {
			vm.reload();
		},
		reload: function () {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'phone': vm.q.inviteCode},
                page:page
            }).trigger("reloadGrid");
		}
	}
});

function buildCode(id) {
	if(id == null) {
		alert("获取邀请码ID失败");
		return;
	}
	
	confirm('确定要生成选中的邀请码吗？', function() {
		$.ajax({
			type: "POST",
			url: baseURL + "inviteCode/buildCode",
			contentType: "application/json",
			data: JSON.stringify(id),
			success: function(r) {
				if(r.code == 0) {
					alert('操作成功', function() {
						vm.reload();
					});
				} else {
					alert(r.msg);
				}
			}
		});
	});
	
}